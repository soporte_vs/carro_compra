class ImagenJob
  include SuckerPunch::Job

  # Deprecado
  def perform(periodo_id)
    #Launchbg.start("bundle exec rake --trace environment 'integra:imagenes_materiales[#{@periodo.id}]'")
    @periodo = Periodo.find(periodo_id)
    #descargamos el archivo a cache
    @periodo.batch.cache!
    Dir.mkdir("#{Rails.root}/tmp") unless File.directory?("#{Rails.root}/tmp")
    Dir.mkdir("#{Rails.root}/tmp/codigo") unless File.directory?("#{Rails.root}/tmp/codigo")

    Zip::ZipFile.open(@periodo.batch.current_path) do |zipfile|
      zipfile.each do |file|
        if file.file? and !file.to_s.include?("__MACOSX")
          codigo = File.basename(file.to_s,File.extname(file.to_s))
          puts codigo
          materiales = Material.where(:codigo => codigo)
          first = true
          for m in materiales
            puts "procesando #{m.id} -> #{m.codigo}"
            if first
              zipfile.extract(file, "#{Rails.root}/tmp/codigo/#{File.basename(file.to_s)}"){ true }
              f = File.open("#{Rails.root}/tmp/codigo/#{File.basename(file.to_s)}")
              m.image.store!(f)
              first = false
            end
            m.has_picture = true
            m.save!
          end
        end
      end
    end

    @periodo.batch_procesando = false
    @periodo.save!
  end

end
