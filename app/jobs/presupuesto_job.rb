class PresupuestoJob
  include SuckerPunch::Job

  # Deprecado todos los metodos
  def perform(periodo_id)
    @periodo = Periodo.find(periodo_id)
    begin
        procesar(@periodo.planilla.url)
    rescue Exception => e
      puts e
      @periodo.status = 7
      @periodo.save!
    end
    Rails.cache.clear
  end

  def procesar(file)
    @periodo.transaction do
      if @periodo.tipo == 1
        procesar_periodo_tipo_1(file)
      elsif @periodo.tipo == 2
        procesar_periodo_tipo_2(file)
      else
        procesar_periodo_tipo_3(file)
      end
    end
  end

  def read_file(file)
    begin
      return Roo::Excelx.new(file)
    rescue TypeError
      return Roo::Excel.new(file)
    end
  end

  private
  def buscaNivelesMateriales(input,row)
    output = []
    #columnas de niveles materiales
    for j in 'G'..'V'
      nivel = input.cell(1,j)
      if !nivel.nil? and nivel.size >= 2 and !nivel.downcase.include? 'precio'  and !nivel.downcase.include? 'presupuesto'
        val = input.cell(row,j)
        if val.to_i > 0
          output << nivel
        end
      end
    end
    return output
  end

  def buscaNivelesJardin(input,row,hoja)
    output = []
    for j in 'F'..'U'
      nivel = input.cell(4,j,hoja)
      if !nivel.nil? and nivel.size >= 2 and !nivel.downcase.include? 'precio'  and !nivel.downcase.include? 'presupuesto'
        val = input.cell(row,j,hoja)
        if val.to_i > 0
          output << nivel
        end
      end
    end
    return output
  end


  def procesar_periodo_tipo_2(file)
    input = read_file(file)
    hojas = input.sheets

    #materiales
    puts 'materiales'
    materiales_id = []
    materiales_count = 0
    precio_en_primera_hoja = false
    columna_precio = nil

    headers = input.row(1)
    for t in 0..(headers.size-1)
      test = headers[t]
      if !test.nil? and test.downcase.include? "precio"
        precio_en_primera_hoja = true
        columna_precio = t+1
      end
    end

    puts "columna precio: #{columna_precio}"


    for i in 2..input.last_row(hojas[0])
      codigo = input.cell(i,'B',hojas[0])
      if codigo.length > 2
        m = Material.where(:periodo_id => @periodo.id, :codigo => codigo).first
        if m.nil?
          m = Material.new
          m.codigo = codigo
          m.periodo_id = @periodo.id
          m.precio_neto = 0
        end
        m.precio_neto = input.cell(i,columna_precio,hojas[0]) if precio_en_primera_hoja
        m.coeficienteTecnico = input.cell(i,'C',hojas[0])
        m.producto = input.cell(i,'D',hojas[0])
        m.caracteristicas = input.cell(i,'E',hojas[0])
        m.unidad = input.cell(i,'F',hojas[0])
        m.categoria_id = Categoria.buscar(input.cell(i,'A',hojas[0])).id
        m.save!
        materiales_count += 1
        MaterialNivel.where(:material_codigo => codigo).delete_all
        niveles = buscaNivelesMateriales(input,i)
        niveles.each do |nivel|
          mn = MaterialNivel.new
          mn.material_codigo = m.codigo
          mn.nivel = nivel
          mn.save!
        end
      end

    end
    min_column = 20+2
    max_column = input.last_column(hojas[1])
    last = 0

    if !precio_en_primera_hoja
      puts "precios de los materiales"
      #precios de los materiales
      precios_count = 0
      for col in min_column..max_column
        codigo = input.cell(3,col,hojas[1])
        if codigo && codigo.size > 1
          precio = input.cell(2,col,hojas[1]).to_f
          m = Material.where(:periodo_id => @periodo.id, :codigo => codigo).first
          throw "Error de integridad" if m.nil?
          m.precio_neto = precio
          m.save!
          precios_count += 1
          last = col
        end
      end
      max_column = last
      raise "error de integridad: precios != materiales " if precios_count != materiales_count
    end

    puts "eliminando la asociacion de jardin nivel"

    JardinNivel.delete_all

    puts "jardines"

    columna_presupuesto = nil

    headers = input.row(4,hojas[1])
    for test in 0..(headers.size-1)
      if headers[test].downcase.include? 'presupuesto'
        columna_presupuesto = test+1
      end
    end

    puts "columna_presupuesto: #{columna_presupuesto}"

    raise 'no se encuentra columna presupuesto' if columna_presupuesto.nil?

    #jardines
    for row in 5..input.last_row(hojas[1])
      codigo = input.cell(row,'C',hojas[1]).to_i

      if codigo && codigo > 0
        puts codigo
        jardin = Jardin.find_by_codigo(codigo)
        if jardin.nil?
          jardin = Jardin.new
          jardin.codigo = codigo
        end
        jardin.nombre = input.cell(row,'D',hojas[1]).capitalize
        jardin.modalidad_atencion = input.cell(row,'E',hojas[1])
        jardin.region = input.cell(row,'A',hojas[1]).to_i
        jardin.comuna = input.cell(row,'B',hojas[1])
        jardin.activo = 1
        jardin.save!


        niveles = buscaNivelesJardin(input,row,hojas[1])
        puts "niveles: #{niveles.join('-')}"
        niveles.each do |nivel|
          jn = JardinNivel.new
          jn.jardin_codigo = jardin.codigo
          jn.nivel = nivel
          jn.save!
        end

        #solicitud
        s = Solicitud.where(:periodo_id => @periodo.id, :jardin_codigo => jardin.codigo).first
        if s.nil?
          puts "creando solicitud jardin_codigo: #{jardin.codigo}, periodo_id: #{@periodo.id}"
          s = Solicitud.new
          s.periodo_id = @periodo.id
          s.jardin_codigo = jardin.codigo
          s.save!
        end
        detalles_ids = []
        for col in min_column..max_column
          codigo = input.cell(3,col,hojas[1])
          m = Material.where(:periodo_id => @periodo.id, :codigo => codigo).first
          detalle = Detalle.where(:solicitud_id => s.id, :material_id => m.id).first
          if detalle.nil?
            detalle = Detalle.new
            detalle.solicitud_id = s.id
            detalle.material_id = m.id
          end

	  #detalle["cantidad_#{headers[col].downcase}"] = input.cell(row,col,hojas[1]).to_i 
	  
          detalle.cantidad_propuesta = input.cell(row,col,hojas[1]).to_i
          detalle.save!
          puts "detalle para material: #{m.id} solicitud: #{s.id} propuesto: #{detalle.cantidad_propuesta}"
          detalles_ids << detalle.id
        end
        d = Detalle.where(:solicitud_id => s.id)
        if detalles_ids.size > 0
          d = d.where("id not in (?)",detalles_ids)
        end
        d.delete_all


        p = Presupuesto.where(:jardin_id => jardin.id, :periodo_id => @periodo.id).first
        if p.nil?
          p = Presupuesto.new
          p.jardin_id = jardin.id
          p.periodo_id = @periodo.id
        end
        p.presupuesto = input.cell(row,columna_presupuesto,hojas[1])
        p.save!

        puts "presupuesto: #{p.presupuesto}"

      end
    end

    @periodo.status = 4
    @periodo.save!

  end

  def check_if_picture_exists(material_id)
    require 'rest-client'
    begin
      RestClient.head Material.find(material_id).square_image
      puts "imagen existe: #{material_id} -> #{Material.find(material_id).square_image}"
      return true
    rescue
      puts "imagen no existe"
      return false
    end
  end

  def procesar_periodo_tipo_3(file)
    procesar_periodo_tipo_2(file)
  end

  def procesar_periodo_tipo_1(file)
    input = nil
    begin
      input = Roo::Excelx.new(file)
    rescue TypeError
      begin
        begin
          input = Roo::Excel.new(file)
        rescue TypeError
          puts "error al procesar el tipo de archivo"
          @periodo.status = 7
          @periodo.save!
          return
        end
      end
    end
    #materiales
    materiales_id = []
    for i in input.first_column..input.last_column
      if input.cell(1, i).to_f > 0
        codigo = input.cell(2, i).strip.gsub(/\s/, '')
        nombre = input.cell(3, i).strip
        precio = input.cell(1, i).to_f
        m = Material.where(['periodo_id = ? and codigo = ?', @periodo.id, codigo]).first
        if m.nil?
          m = Material.new
          m.codigo = codigo
          m.periodo_id = @periodo.id
        end
        m.producto = nombre
        m.precio_neto = precio
        m.categoria_id = Categoria.find(7).id
        m.save!
        m.has_picture = check_if_picture_exists(m.id)
        m.save!
        materiales_id << m.id
      end
    end
    #materiales que ya no se necesitan
    Material.where('periodo_id = ? and id not in (?)', @periodo.id, materiales_id).delete_all

    #jardines
    solicitudes_ids = []
    for i in 4..input.last_row
      codigo = input.cell(i, 3).to_i
      puts "codigo: #{codigo}"
      break unless codigo > 0
      codigo = codigo.to_s
      jardin = Jardin.find_by_codigo(codigo)
      if jardin.nil?
        nombre = input.cell(i, 4).strip
        modalidad = input.cell(i, 5).strip
        comuna = input.cell(i, 2)
        comuna = "NA" if comuna.nil?
        comuna = comuna.strip
        region = input.cell(i, 1).to_i
        jardin = Jardin.new
        jardin.codigo = codigo
        jardin.region = region
        jardin.comuna = comuna
        jardin.nombre = nombre
        jardin.modalidad_atencion = modalidad
      end
      jardin.activo = 1
      jardin.save!
      s = Solicitud.where('jardin_codigo = ? and periodo_id = ?', jardin.codigo, @periodo.id).first
      if s.nil?
        s = Solicitud.new
        s.jardin_codigo = jardin.codigo
        s.periodo_id = @periodo.id
        s.save!
      end
      puts "#{s.jardin_codigo}: #{s.id}"
      solicitudes_ids << s.id
      s.load_detalles
      #presupuesto = 0
      detalles_id = []
      max_column = 0
      for j in input.first_column..input.last_column
        if input.cell(1, j).to_f > 0
          max_column = [j,max_column].max
          codigo = input.cell(2, j).strip.gsub(/\s/, '')
          m = Material.buscar(codigo, @periodo.id)
          detalle = s.detalle_from_cache(m.id)
          if detalle.nil?
            detalle = Detalle.new
            detalle.solicitud_id = s.id
            detalle.material_id = m.id
          end
          detalle.cantidad_propuesta = input.cell(i, j).to_i
          detalle.save!
          detalles_id << detalle.id
          #presupuesto += detalle.cantidad_propuesta * m.precio_neto
        end
      end
      Detalle.where('solicitud_id = ? and id not in (?)', s.id, detalles_id).delete_all unless detalles_id.size == 0
      s.save!
      p = Presupuesto.where(['jardin_id = ? and periodo_id = ?', jardin.id, @periodo.id]).first
      if p.nil?
        p = Presupuesto.new
        p.jardin_id = jardin.id
        p.periodo_id = @periodo.id
      end
      p.presupuesto = input.cell(i,max_column+1).to_i
      p.save!
    end
    Solicitud.where('periodo_id = ? and id not in (?)', @periodo.id, solicitudes_ids).delete_all unless solicitudes_ids.size == 0
    @periodo.status = 4
    @periodo.save!
  end

end
