class EmailJob
  include SuckerPunch::Job


  # Deprecado
  def perform(req)
    ActiveRecord::Base.connection_pool.with_connection do
      s = Solicitud.find(req[:solicitud_id])
      debug = "Agregando mail en cola. Solicitud #{req[:solicitud_id]} - email: #{s.email} - req: #{req[:type]}"
      Rails.logger.info(debug)
      puts debug
      begin
        if (req[:type] == "aprobar")
          ::Notifier.aprobar(req[:solicitud_id]).deliver
        end
        if (req[:type] == "rechazar")
          ::Notifier.rechazar(req[:solicitud_id]).deliver
        end
        if (req[:type] == "confirmar")
          ::Notifier.confirmation(req[:solicitud_id]).deliver
        end
      rescue Exception => e
        Rails.logger.error("Imposible mandar el correo (JOB 1) #{req[:solicitud_id]}")
        Rails.logger.error(e)
        puts "Imposible mandar el correo (JOB 2) #{req[:solicitud_id]}"
        puts e
      end
      puts "correo enviado"
    end
  end
end
