# -*- coding: utf-8 -*-
class PeriodoValidator < ActiveModel::Validator
  def validate(record)
    if record.methods.include?(:status) and record.status == 5 and record.materiales.size() == 0
      record.errors[:base] << 'No es posible activar un periodo de postulación sin materiales'
    end
  end
end