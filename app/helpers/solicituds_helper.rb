module SolicitudsHelper
  def safe_image_tag(source, options = {})
      #source ||= "default.jpg"
      begin
        return image_tag(source, options)
      rescue
        return source.split("/")[1]
      end
    end
end
