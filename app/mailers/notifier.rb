# -*- coding: utf-8 -*-

class Notifier < ActionMailer::Base
  default from: 'carro@fundacion.integra.cl'
  

  
  def reporte(email, username,attach)
    date = Time.now.strftime("%Y%d%m%H%M")

    # TODO Fix
    #connection = Fog::Storage.new({
    #                                  :provider                 => 'AWS',
    #                                  :aws_access_key_id        => (ENV['AWS_ACCESS_KEY_ID']).gsub("'", ''),
    #                                  :aws_secret_access_key    => (ENV['AWS_SECRET_ACCESS_KEY']).gsub("'", '')
    #                              })
    #puts "connection ready"
    #directory = connection.directories.create(
    #    :key    => (ENV['FOG_DIRECTORY']).gsub("'", ''), # globally unique name
    #    :public => true
    #)
    #puts "directory created"

    #file = directory.files.create(
    #    :key    => "reporte-#{username}-#{date}.xls",
    #    :body   => attach,
    #    :content_type => 'application/vnd.ms-excel',
    #    :public => true
    #)
    #puts "file created"
    #@reporte = file.public_url

    #attachments[] = {:data => temp.read, :mime_type => "application/zip"}
    #attachments.inline['logo.jpg'] = {:data => File.read(Rails.root.join('app/assets/images/logo_integra.jpg'), :mime_type => "image/jpeg", :encoding => "base64") }

    attachments['reporte.jpg'] = attach
    mail(:to => email, :subject => "[Integra] Reporte #{date}")
  end

  def confirmation(solicitud_id)
	Rails.logger.error("inicio confirmacion!!!!----------------------------------------------------")
    @mailer = true
    @solicitud = Solicitud.find(solicitud_id)
	Rails.logger.error(@solicitud)
    @periodo = @solicitud.periodo
	Rails.logger.error(@periodo)
    @niveles = JardinNivel.buscar_niveles(@solicitud.jardin.codigo)
	Rails.logger.error(@niveles)
    @materiales = @periodo.materiales
	Rails.logger.error(@materiales)
	Rails.logger.error("ANTES DE ENVIAR CORREOOOOOOO")
    mail(:to => @solicitud.email, :subject => "[Integra] Confirmacion de carro de compras. Jardin: #{@solicitud.jardin.nombre}. Proceso #{@periodo.id}")
  end
  
  def aprobar(solicitud_id)
    @mailer = true
    @solicitud = Solicitud.find(solicitud_id.to_i)
    @periodo = @solicitud.periodo
    @niveles = JardinNivel.buscar_niveles(@solicitud.jardin.codigo)
    @materiales = @periodo.materiales
    
    mail(:to => @solicitud.email, :subject => "[Integra] Solicitud APROBADA para jardin infantil #{@solicitud.jardin.nombre}. Proceso #{@periodo.id}")
  end
  
  def rechazar(solicitud_id)
    @mailer = true
    @solicitud = Solicitud.find(solicitud_id.to_i)
    @periodo = @solicitud.periodo

    mail(:to => @solicitud.email, :subject => "[Integra] Solicitud NO aprobada para jardin infantil #{@solicitud.jardin.nombre}. Proceso #{@periodo.id}")
  end

  def test_email
    mail(:to => 'calvarado@4talent.cl', :subject => "Mensaje de prueba")
  end
end
