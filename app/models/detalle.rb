class Detalle < ActiveRecord::Base
  attr_accessible :cantidad_solicitada
  validates_presence_of :material_id
  validates_presence_of :solicitud_id
  belongs_to :material
  belongs_to :solicitud
  validates_numericality_of :cantidad_propuesta, :greater_than_or_equal_to => 0
  validates_numericality_of :cantidad_solicitada, :greater_than_or_equal_to => 0
  
end
