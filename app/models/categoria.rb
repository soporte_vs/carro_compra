class Categoria < ActiveRecord::Base
  attr_accessible :color, :nombre
  has_many :materiales

  def self.buscar(input)
    @test ||= {}
    @test[input] ||= Categoria.where(:nombre => input).first
    @test[input] ||= Categoria.where(['lower(nombre) = ?',input.downcase]).first
    @test[input] ||= Categoria.where(['lower(nombre) = ?',input.downcase.gsub(/material /,'')]).first
    @test[input] ||= Categoria.where(['lower(nombre) = ?',input.downcase.gsub(/material /,'').singularize]).first
    @test[input] ||= Categoria.where(['nombre = ?',input.gsub(/MATERIAL /,'')]).first
    @test[input] ||= Categoria.where(['nombre = ?',input.gsub(/MATERIAL /,'').pluralize.upcase]).first
    return @test[input]
  end

end
