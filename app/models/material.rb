class Material < ActiveRecord::Base
  self.table_name = 'materiales'
  validates_uniqueness_of :codigo, :scope => :periodo_id
  validates_numericality_of :precio_neto, :greater_than_or_equal_to => 0
  attr_accessible :nivel,:codigo, :producto, :precio_neto, :caracteristicas, :coeficienteTecnico, :image,:categoria_id
  belongs_to :categoria, :foreign_key => :categoria_id
  belongs_to :periodo
  has_many :detalles

  mount_uploader :image, LocalImageUploader
  
  before_destroy { |record| MaterialNivel.destroy_all "material_codigo = '#{record.codigo}'"   }



  def self.buscar(codigo,periodo_id)
    @buscar ||= Hash.new
    @buscar[periodo_id] ||= Hash.new
    @buscar[periodo_id][codigo] ||= Material.where('codigo = ? and periodo_id = ?', codigo, periodo_id).first
  end


  def thumb_image
    return self.image.thumb.url
    #return "#{ASSET_HOST}/materiales/thumb_#{self.codigo_clean}.jpg"
  end
  
  def square_image
    return self.image.square.url
    #return "#{ASSET_HOST}/materiales/square_#{self.codigo_clean}.jpg"
  end
  
  def nombre
    return self.producto.gsub("ARTEL ","")
  end
  
  def nombre_categoria
	  @nombre_categoria ||= self.categoria.nombre if self.categoria
  end
  
  def codigo_clean
    return self.codigo.gsub("-",'').downcase.gsub(" ",'')
  end
  
  def image_resized
    return "resized/" + self.codigo.gsub("-",'') + ".JPG"
  end
  
  def image_original
    return "original/" + self.codigo.gsub("-",'') + ".JPG"
  end
  
  def self.cantidad
    cantidad = Rails.cache.read("cantidad-materiales")
    if cantidad.nil?
      cantidad = find(:all, :conditions => ["periodo_id = ? and materiales.codigo <> '02-ESC-04-1508' ",PERIODO], :order => 'producto ASC').size
      Rails.cache.write("cantidad-materiales",cantidad)
    end
    return cantidad
  end
  
  def nivel=(niveles)
    puts "niveles es #{niveles}"
    puts "keys es #{niveles.keys}"
    ax = MaterialNivel.find_all_by_material_codigo(self.codigo)
    for i in ax
      i.destroy
    end
    for i in niveles.keys
      if NIVELES.include? i
        mn = MaterialNivel.new
        mn.material_codigo = self.codigo.dup
        mn.nivel = i.dup
        mn.save!
      else
        puts "NIVELES no incluye #{i}"
      end
    end
  end
  
  def niveles    
    aux = MaterialNivel.find_all_by_material_codigo(self.codigo)
    output = []
    for i in aux
      output << i.nivel
    end
    return output
  end
  
  def has_nivel(nivel)
    return self.niveles.include? nivel
  end

  def self.image_job(periodo_id)
    #Launchbg.start("bundle exec rake --trace environment 'integra:imagenes_materiales[#{@periodo.id}]'")
    @periodo = Periodo.find(periodo_id)
    #descargamos el archivo a cache
    @periodo.batch.cache!
    Dir.mkdir("#{Rails.root}/tmp") unless File.directory?("#{Rails.root}/tmp")
    Dir.mkdir("#{Rails.root}/tmp/codigo") unless File.directory?("#{Rails.root}/tmp/codigo")

    Zip::ZipFile.open(@periodo.batch.current_path) do |zipfile|
      zipfile.each do |file|
        if file.file? and !file.to_s.include?("__MACOSX")
          codigo = File.basename(file.to_s,File.extname(file.to_s))
          puts codigo
          materiales = Material.where(:codigo => codigo)
          first = true
          for m in materiales
            puts "procesando #{m.id} -> #{m.codigo}"
            if first
              zipfile.extract(file, "#{Rails.root}/tmp/codigo/#{File.basename(file.to_s)}"){ true }
              f = File.open("#{Rails.root}/tmp/codigo/#{File.basename(file.to_s)}")
              m.image.store!(f)
              first = false
            end
            m.has_picture = true
            m.save!
          end
        end
      end
    end

    @periodo.batch_procesando = false
    @periodo.save!
  end

  
end
