class Jardin < ActiveRecord::Base
   validates_uniqueness_of :codigo
   validates_presence_of :codigo, :comuna, :nombre
   validates_numericality_of :codigo, :greater_than => 0
   validates_numericality_of :region, :greater_than => 0, :less_than_or_equal_to => 16

   self.table_name =  'jardines'
   alias_attribute 'modalidadAtencion', 'modalidad_atencion'
   attr_accessible :codigo, :region, :comuna, :nombre, :modalidadAtencion, :presupuesto, :nivel
   has_many :solicitudes, :primary_key => 'codigo', :foreign_key => 'jardin_codigo'
   has_many :usuarios
   has_many :jardin_nivels
   has_many :presupuestos
   has_many :niveles, :through => :jardin_nivels
   before_destroy :eliminar_solicitud

   def nivel=(niveles)
     ax = JardinNivel.find_all_by_jardin_codigo(self.codigo)
     for i in ax
       i.destroy
     end
     for i in niveles.keys
       if NIVELES.include? i
         jn = JardinNivel.new
         jn.jardin_codigo = self.codigo.to_s.dup
         jn.nivel = i.dup
         jn.save!
       else
         puts "NIVELES no incluye #{i}"
       end
     end
   end   
   
   def niveles    
     aux = JardinNivel.find_all_by_jardin_codigo(self.codigo)
     output = []
     for i in aux
       output << i.nivel
     end
     return output
   end

   def has_nivel(nivel)
     return self.niveles.include? nivel
   end

   def solicitud(periodo_id)
     @solicitud ||= {}
     @solicitud[periodo_id] ||= self.solicitudes.where(["periodo_id = #{periodo_id.to_i}"]).first
   end

   def presupuesto(periodo_id)
    s = self.presupuestos.where(["periodo_id = #{periodo_id.to_i}"]).first
    if s.nil?
      return "N/A"
    else
      return s.presupuesto
    end
   end
   
   #def presupuesto
   #  s = self.solicitud(PERIODO)
   #  if s.nil?
   #    return 0
   #  else
   #    return s.presupuesto
   #  end
   #end
   #
   #def presupuesto=p
   #  s = self.solicitud(PERIODO)
   #  if s.nil? and self.save
   #    s = Solicitud.new
   #    s.jardin_id = self.codigo
   #    s.periodo_id = PERIODO
   #  end
   #  unless s.nil?
   #    s.presupuesto = p
   #    s.save(:validate => false)
   #  end
   #end
   
  
   
end
