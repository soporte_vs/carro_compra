class Solicitud < ActiveRecord::Base
  self.table_name = 'solicitudes'
  attr_accessible :email, :observacion_administrador
  validates_presence_of :jardin_codigo
  validates_presence_of :periodo_id
  validates_presence_of :estado

  #un jardin s�lo puede tener una solicitud en un periodo determinado
  validates_uniqueness_of :jardin_codigo, :scope => :periodo_id
  belongs_to :jardin, :primary_key => 'codigo', :foreign_key => 'jardin_codigo'
  validates :email, :email_format => true, :if => Proc.new { |s| s.estado == 1 }
  validates_presence_of :email, :if => Proc.new { |s| s.estado != Solicitud.MODIFICADA and s.estado != 0}
  validates_presence_of :observacion_administrador, :if => Proc.new { |s| s.estado == Solicitud.COMENTARIOS}
  belongs_to :usuario, :foreign_key => 'user_id'
  belongs_to :periodo
  has_many :detalles, :class_name => "Detalle", :order => 'id ASC'

  before_validation :set_estado_nombre

  attr_accessible :observacion_solicitante

  before_save :calcular_total_solicitado

  after_save :clear_cache


  def clear_cache
    Rails.cache.delete("jardin:#{self.jardin_codigo}:periodo:#{self.periodo_id}")
  end
  
  #def detalles
  #  return Detalle.find(:all, :include => [:material], :conditions => ["solicitud_id = ? and materiales.codigo <> '02-ESC-04-1508' ", self.id], :order => "materiales.producto")
  #end
  
  def detalle_for_material(material)
    if self.id.nil?
      d = Detalle.new
      d.cantidad_solicitada = 0
      return d
    else
      #d = Detalle.find(:first, :include => [:material], :conditions => ["solicitud_id = ? and materiales.id = ?", self.id, material.id])
      d = self.detalles.where(["material_id = ?",material.id]).first
      if d.nil?
        d = Detalle.new
        d.material_id = material.id
        d.solicitud_id = self.id
        d.cantidad_solicitada = 0
      end
      return d
    end
  end


  def estado_nombre_usuario
    return "NO MODIFICADA" if self.estado_nombre == 'CREADA'
    self.estado_nombre
  end
    
  def set_estado_nombre
    case self.estado
    when Solicitud.CREADA
      self.estado_nombre = "CREADA"
    when Solicitud.ENVIADA
      self.estado_nombre =  "ENVIADA"
    when Solicitud.MODIFICADA
      self.estado_nombre =  "MODIFICADA"
    when Solicitud.COMENTARIOS
      self.estado_nombre =  "CON COMENTARIOS"
    when Solicitud.APROBADA
      self.estado_nombre =  "APROBADA"
    end
  end
  
  def self.CREADA
    0
  end
  
  def self.MODIFICADA
    2
  end
  
  def self.ENVIADA
    1
  end
  
  def self.COMENTARIOS
    3
  end
  
  def self.APROBADA
    4
  end
  
  def en_plazo
    self.periodo.en_plazo?
  end

  def calcular_total_solicitado
    return 0.0 if self.estado == Solicitud.CREADA
    total = 0.0
    for d in self.detalles
      total += 1.0*d.material.precio_neto * d.cantidad_solicitada
    end
    self.costo_total = total.round
  end


  def self.count_by_region_estado_periodo(region,periodo_id, estado)
    Jardin.count_by_sql("select count(*) from jardines where region = #{region} and activo = 1 and codigo IN (select jardin_codigo from solicitudes where periodo_id = #{periodo_id.to_i} and estado = #{estado})")
  end

  def self.count_by_estado_periodo(periodo_id, estado)
    Jardin.count_by_sql("select count(*) from jardines where activo = 1 and codigo IN (select jardin_codigo from solicitudes where periodo_id = #{periodo_id.to_i} and estado = #{estado})")
  end

  def presupuesto
	pr = Presupuesto.where(['jardin_id = ? and periodo_id = ?',self.jardin.id,self.periodo_id])
	@presupuesto ||= pr.first.presupuesto if pr.any?
  end

  def load_detalles
    @detalle = Hash.new
    for i in self.detalles
      @detalle[i.material_id] = i
    end
  end

  def detalle_from_cache(material_id)
    #precaucion, antes de usar este metodo debe ser precargado el cache con load_detalles
    @detalle[material_id]
  end

  def self.email_job(req)
    ActiveRecord::Base.connection_pool.with_connection do
      s = Solicitud.find(req[:solicitud_id])
      debug = "Agregando mail en cola. Solicitud #{req[:solicitud_id]} - email: #{s.email} - req: #{req[:type]}"
      Rails.logger.info(debug)
      puts debug
      begin
        if (req[:type] == "aprobar")
          ::Notifier.aprobar(req[:solicitud_id]).deliver
        end
        if (req[:type] == "rechazar")
          ::Notifier.rechazar(req[:solicitud_id]).deliver
        end
        if (req[:type] == "confirmar")
          ::Notifier.confirmation(req[:solicitud_id]).deliver
        end
      rescue Exception => e
        Rails.logger.error("Imposible mandar el correo (SOlicitud 1) #{req[:solicitud_id]}")
        Rails.logger.error(e)
        puts "Imposible mandar el correo (SOlicitud 2) #{req[:solicitud_id]}"
        puts e
      end
      puts "correo enviado"
    end
  end

  def self.reporte_job(user_id, periodo_id, email)
    #Launchbg.start("bundle exec rake 'integra:excel[#{current_user.id},#{@periodo.id},#{params[:email]}]'")
    @periodo = Periodo.find(periodo_id)
    @materiales = @periodo.materiales
    @solicitudes = nil
    u = Usuario.find(user_id)
    if (u.isAdmin?)
      @solicitudes = @periodo.solicitudes
    else
      if (u.region > 0)
        @solicitudes = Solicitud.find(:all, :include => [:jardin],:conditions => ["periodo_id = #{@periodo.id} and jardines.region = ?",u.region], :order => "jardin_codigo ASC")
      else
        @solicitudes = Solicitud.find(:all, :include => [:jardin],:conditions => ["periodo_id = #{@periodo.id} and jardines.codigo = ?",u.jardin_codigo], :order => "jardin_codigo ASC")
      end
    end

    av = ActionView::Base.new(Integra::Application.config.paths["app/views"].first)
    xls = av.render(:partial => "solicitudes/xls", :format => 'xml', :locals => {:materiales => @materiales, :solicitudes => @solicitudes})

	puts "MODEL SOLICITUD - REPORTE JOB"
    Notifier.reporte(email,user_id.to_i,xls).deliver
    puts "enviado"
  end
end
