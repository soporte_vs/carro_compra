class Presupuesto < ActiveRecord::Base
  attr_accessible :jardin_id, :periodo_id, :presupuesto
  validates_uniqueness_of :jardin_id, :scope => :periodo_id
  belongs_to :jardin
  belongs_to :periodo
  validates_presence_of :jardin_id, :periodo_id, :presupuesto
  validates_numericality_of :presupuesto, :greater_than_or_equal_to => 0

end
