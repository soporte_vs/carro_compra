class MaterialNivel < ActiveRecord::Base
  self.table_name = 'nivel_material'
  belongs_to :material
  validates_uniqueness_of :nivel, :scope => :material_codigo

  
end
