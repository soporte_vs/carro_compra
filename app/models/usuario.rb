class Usuario < ActiveRecord::Base
  has_secure_password
  has_many :solicitudes, :class_name => "Solicitud"
  belongs_to :jardin, :foreign_key => "jardin_codigo", :primary_key => "codigo"
  attr_accessible :username,:nombre, :jardin_id, :region, :nombre,:password, :password_confirmation
  
  validates_uniqueness_of :username
  validates_presence_of :username
  before_validation :username_must_not_be_zero

  def username_must_not_be_zero
    self.username = self.username.strip
    return false if self.username.to_s == '0'
  end

  # attr_accessible :title, :body
  
  def isAdmin?
    return self.admin == 1
  end
  
  def username=(t)
      unless t.nil? or t.to_s.empty?
      write_attribute('username',t.to_s.split("-")[0].gsub(".",""))
    end
  end

  def self.actualizar_usuarios(file)
    input = nil
    begin
      input = Roo::Excelx.new(file)
    rescue TypeError
      begin
        input = Roo::Excel.new(file)
      rescue TypeError
        render :text => "formato no reconocido. Planilla debe venir en formato XLSX"
        return
      end
    end

    Usuario.connection.execute('UPDATE usuarios set valido = false, autoriza = false where admin = 0')
    for i in 2..input.last_row
      region = input.cell(i,'A').to_i
      codigo = input.cell(i,'D').to_i
      nombre = input.cell(i,'I')
      username = input.cell(i,'G').to_i.to_s
      puts "#{username} -> #{codigo}"


      jardin = Jardin.find_by_codigo(codigo)
      if jardin.nil?
        jardin = Jardin.new
        jardin.nombre = input.cell(i,'E')
        jardin.codigo = codigo
        jardin.region = region
        jardin.comuna = input.cell(i,'C')
        jardin.modalidad_atencion = input.cell(i,'F')
        jardin.save!
      end

      usuario = Usuario.where(:username => username).first
      if usuario.nil?
        usuario = Usuario.new
        usuario.username = username
        if usuario.username.length > 6
          usuario.password = usuario.username[0,4]
        else
          usuario.password = usuario.username
        end
      end
      usuario.nombre = nombre
      usuario.jardin_codigo = codigo
      usuario.region = region
      usuario.valido = true
      usuario.save!

    end
    hojas = input.sheets

    puts "usuarios regionales"
    if hojas.size > 1
      for i in 2..input.last_row(hojas[1])
        region = input.cell(i,'A',hojas[1]).to_i
        username = input.cell(i,'D',hojas[1]).to_i.to_s
        nombre = input.cell(i,'H',hojas[1])

        usuario = Usuario.where(:username => username).first
        if usuario.nil?
          puts "creando #{username}"
          usuario = Usuario.new
          usuario.username = username
          usuario.password = usuario.username[0,4]
        else
          puts "actualizando #{username}"
        end
        usuario.email = input.cell(i,'L',hojas[1])
        usuario.nombre = nombre
        usuario.region = region
        usuario.valido = true
        usuario.autoriza = (input.cell(i,'M',hojas[1]) == 'SI')
        usuario.save!
      end
    end
  end

  def self.usuario_job
    #Launchbg.start("bundle exec rake --trace environment 'integra:procesar_usuarios'")
    file = Carro.first.usuarios.to_s
    @carro = Carro.first
    begin
      Usuario.transaction do
        Usuario.actualizar_usuarios(file)
      end
      @carro.usuarios_status = 0
      @carro.save!
    rescue
      @carro.usuarios_status = 2
      @carro.save!
    end
  end
end
