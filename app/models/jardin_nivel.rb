class JardinNivel < ActiveRecord::Base
  self.table_name = 'nivel_jardin'
  belongs_to :jardin, :foreign_key => "jardin_codigo", :primary_key => "codigo"
  validates_presence_of :jardin_codigo, :nivel
  attr_accessible :nivel
  
  def self.buscar_niveles(j)
    aux = JardinNivel.find_all_by_jardin_codigo(j)
    output = []
    for i in aux
      output << "'#{i.nivel}'"
    end
    return output
  end
end
