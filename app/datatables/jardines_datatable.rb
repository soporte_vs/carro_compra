# encoding: utf-8
class JardinesDatatable
  delegate :params, :h, :link_to, :number_to_currency, to: :@view
  
  def initialize(view,current_user,estado,periodo_id)
    @view = view
    @current_user = current_user
    @estado = estado
    @periodo = Periodo.find(params[:periodo_id])
    #puts "initialize: estado: #{@estado}"
  end
  
  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Jardin.joins(:solicitudes).where(['periodo_id = ?',@periodo.id]).count,
      iTotalDisplayRecords: jardines.total_entries,
      aaData: data
    }
  end
  
private

  def link_or_create_solicitud(jardin,periodo)
    s = jardin.solicitud(periodo)
    if (s.nil?)
      link_to jardin.nombre, "/solicitudes/new/#{jardin.codigo}", :confirm => "Este jardín no posee una solicitud, ¿desea crear una?"
    else
      link_to(jardin.nombre, jardin.solicitud(periodo))

    end
  end

  def buscar_en_cache(jardin)
    mem = Rails.cache.read("jardin:#{jardin.id}:periodo:#{@periodo.id}")
    if mem.nil?
      mem = [
          jardin.solicitud(@periodo.id).nil?? 'No existe':jardin.solicitud(@periodo.id).estado_nombre,
          jardin.region,
          jardin.comuna,
          jardin.codigo,
          jardin.solicitud(@periodo.id).nil?? jardin.nombre : link_to(jardin.nombre, "/solicitudes/#{jardin.solicitud(@periodo.id).id}"),
          jardin.modalidad_atencion,
          number_to_currency(jardin.presupuesto(@periodo.id) , {:precision => 0,:delimiter => '.'}),
          jardin.solicitud(@periodo.id).nil?? '0':number_to_currency(jardin.solicitud(@periodo.id).costo_total, {:precision => 0,:delimiter => '.'} ),
          "#{jardin.solicitud(@periodo.id).nil?? '0':jardin.solicitud(@periodo.id).id}##{jardin.id}##{@periodo.id}"
      ]
      Rails.cache.write("jardin:#{jardin.id}:periodo:#{@periodo.id}",mem)
    end
    return mem
  end

  def data
    jardines.map do |jardin|
      [
          jardin.solicitud(@periodo.id).nil?? 'No existe':jardin.solicitud(@periodo.id).estado_nombre,
          jardin.region,
          jardin.comuna,
          jardin.codigo,
          jardin.solicitud(@periodo.id).nil?? jardin.nombre : link_to(jardin.nombre, "/solicitudes/#{jardin.solicitud(@periodo.id).id}"),
          jardin.modalidad_atencion,
          number_to_currency(jardin.presupuesto(@periodo.id) , {:precision => 0,:delimiter => '.'}),
          jardin.solicitud(@periodo.id).nil?? '0':number_to_currency(jardin.solicitud(@periodo.id).costo_total, {:precision => 0,:delimiter => '.'} ),
          "#{jardin.solicitud(@periodo.id).nil?? '0':jardin.solicitud(@periodo.id).id}##{jardin.id}##{@periodo.id}"
      ]
    end
  end
    
  def jardines
    @jardines ||= fetch_jardines
  end
  
  def fetch_jardines
    jardines = Jardin.joins(:solicitudes).where(['periodo_id = ?',@periodo.id])
    if @current_user.region > 0
      jardines = jardines.where(["jardines.region = ?",@current_user.region])
    end
    if params[:sSearch].present?
      jardines = jardines.where('nombre ilike :search or estado_nombre ilike :search or comuna ilike :search or region = :searchNumero or codigo = :searchNumero', {search: "%#{params[:sSearch]}%", searchNumero: params[:sSearch].to_i})
    end
    if params[:pendientes].present? and params[:pendientes] == "true"
      jardines = jardines.where("solicitudes.estado = 1")
    end
    jardines = jardines.order("#{sort_column} #{sort_direction}")
    jardines = jardines.page(page).per_page(per_page)
    jardines

  end
    
  def page
    params[:iDisplayStart].to_i/per_page + 1
  end
  
  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end
  
  def sort_column
    columns = %w[estado_nombre jardines.region jardines.comuna jardines.codigo jardines.nombre jardines.modalidad_atencion presupuesto costo_total nombre]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc": "asc"
  end
    
  
  
end