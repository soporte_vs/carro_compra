# encoding: utf-8

class BatchUploader < CarrierWave::Uploader::Base
  include CarrierWaveDirect::Uploader
  include Sprockets::Helpers::RailsHelper
  include Sprockets::Helpers::IsolatedHelper

  def extension_white_list
    %w(zip)
  end

  def store_dir
    "/batch"
  end



end
