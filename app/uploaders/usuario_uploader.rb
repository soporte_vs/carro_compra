# encoding: utf-8

class UsuarioUploader < CarrierWave::Uploader::Base
  include CarrierWaveDirect::Uploader
  include Sprockets::Helpers::RailsHelper
  include Sprockets::Helpers::IsolatedHelper

  def extension_white_list
    %w(xlsx)
  end

  def store_dir
    "/uploads"
  end


end
