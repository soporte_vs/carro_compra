class MaterialesController < ApplicationController
   before_filter :admin_permission, :except => :modal

  def modal
    @material = Material.find(params[:id].to_i)
    render :layout => false
  end



  # GET /materiales
  # GET /materiales.json
  def index
    @periodo = Periodo.find(params[:periodo_id])
    if params[:key] and @periodo.batch_procesando == false
      @uploader = @periodo.batch
      @uploader.key = params[:key]
      @periodo.batch_procesando = true
      @periodo.save!
      #ImagenJob.new.async.perform(@periodo.id)
      Material.delay.image_job(@periodo.id)
      
      redirect_to periodo_path(@periodo)
      return
    end
    @materiales = @periodo.materiales
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @materiales }
    end
  end

  def upload
    @periodo = Periodo.find(params[:periodo_id])
    @uploader = @periodo.batch
    @uploader.key = "/batch/#{UUIDTools::UUID.random_create}/batch-#{@periodo.id}.zip"
    @uploader.success_action_redirect = periodo_materiales_url(@periodo)

  end

  # GET /materiales/1
  # GET /materiales/1.json
  def show
    @material = Material.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @material }
    end
  end

  # GET /materiales/new
  # GET /materiales/new.json
  def new
    @material = Material.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @material }
    end
  end

  # GET /materiales/1/edit
  def edit
    @material = Material.find(params[:id])
  end

  # POST /materiales
  # POST /materiales.json
  def create
    @material = Material.new(params[:material])
    @material.periodo_id = params[:periodo_id]

    respond_to do |format|
      if @material.save
        expire_fragment('materiales')
        format.html { redirect_to periodo_materiales_path, notice: 'Material was successfully created.' }
        format.json { render json: @material, status: :created, location: @material }
      else
        format.html { render action: 'new' }
        format.json { render json: @material.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /materiales/1
  # PUT /materiales/1.json
  def update
    @material = Material.find(params[:id])

    respond_to do |format|
      if @material.update_attributes(params[:material])
        expire_fragment("materiales")
        format.html { redirect_to periodo_materiales_path, notice: 'Material was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @material.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /materiales/1
  # DELETE /materiales/1.json
  def destroy
    @material = Material.find(params[:id])
    @material.destroy
    expire_fragment("materiales")

    respond_to do |format|
      format.html { redirect_to materiales_url }
      format.json { head :no_content }
    end
  end
end
