class UsuariosController < ApplicationController
   before_filter :admin_permission

  # GET /usuarios
  # GET /usuarios.json
  def index
    @carro = Carro.first
    if params[:key]
      @uploader = @carro.usuarios
      @uploader.key = params[:key]
      @carro.usuarios_status = 1
      @carro.save!
      UsuarioJob.new.async.perform()

    end
    if @carro.usuarios_status == 1
      render :action => :usuarios_wait
      return
    end


    if (params[:jardin].to_i == 0)
      @usuarios = Usuario.all
      render :action => 'index_usuarios'
    else
      @jardin = Jardin.find_by_codigo(params[:jardin])
      @usuarios = Usuario.where(['jardin_codigo = ? OR (region > 0 and region = ?) and valido = ?', @jardin.codigo,@jardin.region,true]).order('jardin_codigo DESC, nombre ASC').all
      if (!current_user.isAdmin? and current_user.region.to_i != @jardin.region.to_i)
          redirect_to root_url, :alert => 'permiso denegado'
      else
        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: @usuarios }
        end
      end
    end
  end

  def upload
    @carro = Carro.first
    @uploader = Carro.first.usuarios
    @uploader.key = "/uploads/#{UUIDTools::UUID.random_create}/usuarios.xlsx"
    @uploader.success_action_redirect = usuarios_url
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    @usuario = Usuario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/new
  # GET /usuarios/new.json
  def new
    @usuario = Usuario.new
    @usuario.jardin_id = params[:jardin_codigo]
    @jardin = Jardin.find_by_codigo(params[:jardin_codigo])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/1/edit
  def edit
    @usuario = Usuario.find(params[:id])
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    @usuario = Usuario.new(params[:usuario])
    @jardin = Jardin.find_by_codigo(@usuario.jardin_id)
    respond_to do |format|
      if @usuario.save
        format.html {
          if @usuario.isAdmin?
            redirect_to "/solicitudes", notice: "Usuario fue creado exitosamente."
          else
            redirect_to "/jardines/#{@usuario.jardin_id}/usuarios", notice: "Usuario fue creado exitosamente."
          end}
        format.json { render json: @usuario, status: :created, location: @usuario }
      else
        format.html { render action: "new" }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /usuarios/1
  # PUT /usuarios/1.json
  def update
    @usuario = Usuario.find(params[:id])

    respond_to do |format|
      if @usuario.update_attributes(params[:usuario])
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario = Usuario.find(params[:id])
    @usuario.destroy

    respond_to do |format|
      format.html { redirect_to usuarios_url }
      format.json { head :no_content }
    end
  end
end
