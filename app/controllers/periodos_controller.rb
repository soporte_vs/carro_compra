class PeriodosController < ApplicationController
  before_filter :require_valid_user
  before_filter :admin_permission, :except => [:index, :show]
  before_filter :admin_or_regional_permission, :only => :show

  # GET /periodos
  # GET /periodos.json
  def index

    if (current_user.admin == 0)
      @periodos = Periodo.activos
    else
      @periodos = Periodo.order('id asc')
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @periods }
    end
  end

  def subir_listado_materiales
    puts 'subir_listado_mat'
    puts @uploader.inspect
    @periodo = Periodo.find(params[:id])

  end


  # GET /periodos/1
  # GET /periodos/1.json
  def show
    @periodo = Periodo.find(params[:id])

    puts 'LLEGUE'
    if @periodo.status == 0
      if params[:key]
        @uploader = @periodo.planilla
        @uploader.key = params[:key]
        @periodo.status = 3
        @periodo.save!
        PresupuestoJob.new.async.perform(@periodo.id)
        #Periodo.delay.presupuesto_job(@periodo.id)
        redirect_to periodo_url(@periodo)
      else
	puts 'arribauploader'
        @uploader = @periodo.planilla
        @uploader.key = "/uploads/#{UUIDTools::UUID.random_create}/planilla-#{@periodo.id}.xlsx"
        @uploader.success_action_redirect = periodo_url(@periodo)
	puts @uploader.inspect
        render :action => :subir_listado_materiales
      end
    elsif @periodo.status == 3
      render :action => :procesando
    end

  end

  # GET /periodos/new
  # GET /periodos/new.json
  def new
    @period = Periodo.new
    @period.time_start = nil
    @period.time_end = nil

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @period }
    end
  end

  # GET /periodos/1/edit
  def edit
    @period = Periodo.find(params[:id])
  end

  # POST /periodos
  # POST /periodos.json
  def create
    @period = Periodo.new(params[:periodo])


      if @period.save
        redirect_to periodo_url(@period), notice: 'Se ha creado un nuevo carro de compras'
      else
        render action: "new"

      end

  end

  # PUT /periodos/1
  # PUT /periodos/1.json
  def update
    @period = Periodo.find(params[:id])

    if @period.update_attributes(params[:periodo])
      redirect_to periodo_url(@period), notice: 'El periodo fue actualizado exitosamente.'
    else
      render action: 'edit'

    end

  end

  # DELETE /periodos/1
  # DELETE /periodos/1.json
  def destroy
    @period = Periodo.find(params[:id])
    @period.destroy

    respond_to do |format|
      format.html { redirect_to '/periodos' }
      format.json { head :no_content }
    end
  end
end
