class SessionsController < ApplicationController

  def new
    session[:user_id] = nil
  end
  
  def create
    user = Usuario.where(:username => params[:username].gsub(/\./,'').split('-')[0]).first
    unless verify_recaptcha(model: user, secret_key: '6LeKDsYZAAAAAKoax18QE7XOx8LdvjG5yRoGdEBR')
      session[:user_id] = nil
      flash.now.alert = 'Captcha invalido'
      render 'new'
    end
    if user && user.valido? && user.authenticate(params[:password]) 
      session[:user_id] = user.id
      if (user.isAdmin? or user.region > 0)
        redirect_to root_url
      else
        j = user.jardin
        if j.activo == 0
          session[:user_id] = nil
          render :text => 'Lo sentimos, pero su cuenta se encuentra desactivada'
          return
        end
        redirect_to root_url
      end
    else
      session[:user_id] = nil
      flash.now.alert = 'Usuario o contrase&ntilde;a incorrecta'
      render 'new'
    end
  end

  def index
     redirect_to root_url
  end

  def destroy
    session[:user_id] = nil
    reset_session
    redirect_to :action => :new
  end
  

end
