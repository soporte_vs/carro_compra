class JardinesController < ApplicationController
   before_filter :admin_or_regional_permission
   before_filter :admin_permission, :only => [:destroy, :create, :edit]
  # GET /jardines
  # GET /jardines.json
  def index
    #@jardines = Jardin.find(:all, :conditions => ["activo = 1"], :order => "region, codigo")
    # @jardines = Solicitud.find(:all, :include => [:jardin], :conditions => ["periodo_id = #{PERIODO}"], :order => "jardin_id ASC")
    # respond_to do |format|
    #   format.html # index.html.erb
    #   format.json { render json: @jardines }
    # end
    redirect_to "/solicitudes"
  end

  # GET /jardines/1
  # GET /jardines/1.json
  def show
    @jardin = Jardin.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @jardin }
    end
  end

  # GET /jardines/new
  # GET /jardines/new.json
  def new
    @jardin = Jardin.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @jardin }
    end
  end

  # GET /jardines/1/edit
  def edit
    @jardin = Jardin.find(params[:id])
  end

  # POST /jardines
  # POST /jardines.json
  def create
    @jardin = Jardin.new(params[:jardin])
    @jardin.activo = 1
    respond_to do |format|
      if @jardin.save
        format.html { redirect_to "/periodos", notice: "Jardin #{@jardin.nombre} fue creado exitosamente." }
        format.json { render json: @jardin, status: :created, location: @jardin }
      else
        format.html { render action: "new" }
        format.json { render json: @jardin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /jardines/1
  # PUT /jardines/1.json
  def update
    @jardin = Jardin.find(params[:id])

    respond_to do |format|
      if @jardin.update_attributes(params[:jardin])
        format.html { redirect_to root_url, notice: "Jardin #{@jardin.nombre} fue actualizado exitosamente." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @jardin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jardines/1
  # DELETE /jardines/1.json
  #def destroy
  #  @jardin = Jardin.find(params[:id])
  #  ss = Solicitud.find(:all, :conditions => ["jardin_id = ?", @jardin.codigo])
  #  for s in ss
  #    s.destroy unless s.nil?
  #  end
  #  @jardin.destroy
  #
  #  respond_to do |format|
  #    format.html { redirect_to "/solicitudes" }
  #    format.json { head :no_content }
  #  end
  #end
end
