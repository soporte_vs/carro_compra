class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_periodo

  def set_periodo
    @periodo = Periodo.find(params[:periodo_id]) if params[:periodo_id]
  end

  
  private
  
  def current_user
    @current_user ||= Usuario.find(session[:user_id]) if session[:user_id]
  end
  
  def valid_email?(email)
    return email =~ /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i
  end

  def admin_or_regional_permission
    if current_user and (current_user.isAdmin? or current_user.region > 0)
      return true
    else
      reset_session
      redirect_to root_url, :alert => 'permiso denegado'
    end
  end

  def autorizar_permission
    if current_user and current_user.autoriza?
      return true
    else
      reset_session
      redirect_to root_url, :alert => 'permiso denegado'
    end
  end
  
  def admin_permission
    if current_user and (current_user.isAdmin?)
      return true
    else
      reset_session
      redirect_to root_url, :alert => 'permiso denegado'
    end
  end

  def require_valid_user
    if current_user
      return true
    else
      reset_session
      redirect_to :login
    end

  end
  
  
  
  helper_method :current_user
end
