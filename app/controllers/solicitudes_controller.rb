# -*- coding: utf-8 -*-
class SolicitudesController < ApplicationController
  before_filter :require_permission, :except => [:index, :xls, :pendientes, :aprobar, :rechazar]
  before_filter :admin_or_regional_permission, :only => [:index, :xls, :pendientes]
  before_filter :autorizar_permission, :only => [:aprobar, :rechazar]
#  before_filter :set_cache_buster
# GET /solicituds
# GET /solicituds.json

  def aprobar
    @solicitud = Solicitud.find(params[:solicitud_id])
    modo = params[:commit]
    if (modo == "Aprobar")
      @solicitud.estado = Solicitud.APROBADA
    else
      @solicitud.estado = Solicitud.COMENTARIOS
    end
    @solicitud.observacion_administrador = params[:solicitud]['observacion_administrador']
    if @solicitud.save
      if @solicitud.estado == Solicitud.APROBADA
        EmailJob.new.async.perform({:solicitud_id => @solicitud.id, :type => "aprobar"})
      else
        EmailJob.new.async.perform({:solicitud_id => @solicitud.id, :type => "rechazar"})
      end
      redirect_to "/periodos/#{@solicitud.periodo.id}"
    else
      if @solicitud.estado == Solicitud.COMENTARIOS
        redirect_to @solicitud, :alert => "Error al almacenar su solicitud: Solicitud debe incluir un comentario si es que es rechazada."
      else
        redirect_to @solicitud, :alert => "Error"
      end
    end

  end

  def pendientes
    @periodo = Periodo.find(params[:periodo_id])
    @pendientes = true
  end

  #def new
  #  if (current_user.isAdmin? or current_user.region > 0) and params[:jardin_id].present?
  #    j = Jardin.find_by_codigo(params[:jardin_id].to_i)
  #    if (current_user.region > 0 and j.region != current_user.region)
  #      redirect_to "/"
  #      return
  #    else
  #      @solicitud = Solicitud.new
  #      @solicitud.jardin_id = params[:jardin_id].to_i
  #      @solicitud.presupuesto  = j.presupuesto
  #      @solicitud.periodo_id = PERIODO
  #      @solicitud.save
  #    end
  #  else
  #    @solicitud = Solicitud.where(['jardin_codigo = ? and periodo_id = ?',current_user.jardin_codigo,params[:periodo_id]]).first
  #    if @solicitud.nil?
  #      @solicitud = Solicitud.new
  #      @solicitud.jardin_codigo = current_user.jardin_codigo
  #      @solicitud.presupuesto  = Jardin.find_by_codigo(current_user.jardin_id).presupuesto(params[:periodo_id])
  #      @solicitud.periodo_id = params[:periodo_id]
  #      @solicitud.save
  #    else
  #      redirect_to "/solicitudes/#{@solicitud.id}"
  #      return
  #    end
  #  end
  #  redirect_to "/solicitudes/#{@solicitud.id}"
  #end


  # POST /jardines
  # POST /jardines.json
  #def create
  #  @solicitud = Solicitud.new(params[:solicitud])
  #  @solicitud.jardin_id = params['jardin_id']
  #  @solicitud.periodo_id = PERIODO
  #  respond_to do |format|
  #    if @solicitud.save
  #      format.html { redirect_to @solicitud, notice: 'Solicitud was successfully created.' }
  #      format.json { render json: @solicitud, status: :created, location: @solicitud }
  #    else
  #      format.html { render action: "new" }
  #      format.json { render json: @solicitud.errors, status: :unprocessable_entity }
  #    end
  #  end
  #end


  def require_permission
    if current_user.nil?
      reset_session
      redirect_to :login
      return
    end

    if !params['jardin_id'].nil?
      if current_user.jardin_codigo.to_i > 0
        return false if current_user.jardin_codigo.to_i != params['jardin_id']
      end
    end
    if params[:id].nil?
      return current_user.jardin_codigo > 0
    end
    @solicitud = Solicitud.find(:first, :conditions => ["id = ?", params[:id].to_i])
    if !@solicitud.nil? and current_user and current_user.isAdmin?
      return true
    else
      if !@solicitud.nil? and current_user and current_user.region > 0 and @solicitud.jardin.region == current_user.region
        return true
      else
        if !@solicitud.nil? and current_user and (current_user.jardin_codigo == @solicitud.jardin.codigo)
          return true
        else
          reset_session
          redirect_to root_url, :alert => 'permiso denegado'
        end
      end
    end
  end


  def index
    @periodo = Periodo.find(params[:periodo_id])
    respond_to do |format|
      format.json { render json: JardinesDatatable.new(view_context, current_user, nil, @periodo.id) }
    end
  end

  def xls
    @periodo = Periodo.find(params[:periodo_id])
    puts "emails is #{params[:email]}, current_user_id is #{current_user.id}"
    r = false
    if params[:email] =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
      r = true
    end
    puts "email is #{r}"
    if r
      puts "generando xls"
      #ReporteJob.new.async.perform(current_user.id, @periodo.id, params[:email])
      Solicitud.delay.reporte_job(current_user.id, @periodo.id, params[:email])
      
      #Launchbg.start("bundle exec rake 'integra:excel[#{current_user.id},#{@periodo.id},#{params[:email]}]'")
      redirect_to "/periodos/#{@periodo.id}", notice: "El reporte será enviado a su correo #{params[:email]} dentro de los próximos 10 minutos."
    else
      redirect_to "/periodos/#{@periodo.id}", alert: "Error al generar reporte, correo inválido: #{params[:email]}"
    end
  end

  # GET /solicituds/1
  # GET /solicituds/1.json
  def show
    @solicitud = Solicitud.find(params[:id].to_i)
    @periodo = @solicitud.periodo
    @niveles = JardinNivel.buscar_niveles(@solicitud.jardin.codigo)
    if @periodo.tipo == 1
      @materiales = Material.where(['periodo_id = ?', @periodo.id]).order('categoria_id ASC, producto ASC').includes(:categoria)
    elsif @periodo.tipo == 2
      @materiales = Material.where(["periodo_id = ? and ((codigo IN (select material_codigo from nivel_material where nivel IN (#{@niveles.join(',')}))) OR (categoria_id = 7))", @periodo.id]).order("categoria_id ASC, producto ASC")
    elsif @periodo.tipo == 3
      @materiales = Material.where(["periodo_id = ? and ((codigo IN (select material_codigo from nivel_material where nivel IN (#{@niveles.join(',')}))) OR (categoria_id = 7))", @periodo.id]).order("categoria_id ASC, producto ASC")
    end

    #@materiales = Material.where(["periodo_id = ? and codigo IN (select material_codigo from nivel_material where nivel IN (#{@niveles.join(',')}))", @periodo.id]).order("categoria_id ASC, producto ASC")
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @solicitud }
    end

  end

  # PUT /solicituds/1
  # PUT /solicituds/1.json
  def update
    @solicitud = Solicitud.find(params[:id].to_i)
    @solicitud.estado = Solicitud.MODIFICADA unless @solicitud.estado == Solicitud.COMENTARIOS
    Rails.logger.info("UPDATE SOLICITUD #{@solicitud.id} estado #{@solicitud.estado} y modo #{params[:mode]}")
    Detalle.transaction do
      #seteamos todo en cero para el caso de un material que antes existía y luego no le aparece al usuario
      materiales = Detalle.find(:all, :conditions => ["solicitud_id = ?", @solicitud.id])
      for m in materiales
        m.cantidad_solicitada = 0
        m.save
      end

      #revisamos la lista de materiales solicitados y seteamos las cantidades solicitadas
      for i in params[:detalle_scme].keys
        d = Detalle.find(:first, :conditions => ["material_id = ? and solicitud_id = ?", i, @solicitud.id])
        if d.nil?
          d = Detalle.new
          d.solicitud_id = @solicitud.id
          d.material_id = i.to_i
          d.cantidad_propuesta = 0
        end
	# 10 nuevas columnas agregadas
	d.cantidad_scme = params[:detalle_scme][i].to_i
        d.cantidad_scma = params[:detalle_scma][i].to_i
        d.cantidad_sc = params[:detalle_sc][i].to_i
        d.cantidad_mme = params[:detalle_mme][i].to_i
        d.cantidad_mma = params[:detalle_mma][i].to_i
        d.cantidad_med = params[:detalle_med][i].to_i
        d.cantidad_tme = params[:detalle_tme][i].to_i
        d.cantidad_tma = params[:detalle_tma][i].to_i
        d.cantidad_tra = params[:detalle_tra][i].to_i
        d.cantidad_het = params[:detalle_het][i].to_i
	# 28032019 se agrego la nueva columna
	d.cantidad_ji = params[:detalle_ji][i].to_i
	# fin 10 columnas agregadas

	# se debe realizar la suma de las columnas anteriores
        d.cantidad_solicitada = d.cantidad_ji + d.cantidad_het + d.cantidad_tra + d.cantidad_tma + d.cantidad_tme + d.cantidad_med + d.cantidad_mma + d.cantidad_mme + d.cantidad_sc + d.cantidad_scma + d.cantidad_scme

        logger.warn("cantidad solicitada #{d.cantidad_solicitada}")
        puts "cantidad solicitada #{d.cantidad_solicitada}"
        d.save!
      end

    end
    @solicitud.save
    if params[:mode] == 'terminar'
      if @solicitud.presupuesto >= @solicitud.costo_total and @solicitud.en_plazo and @solicitud.estado != Solicitud.APROBADA
        expire_fragment("estado-solicitud-#{params[:id]}")
        @solicitud.estado = Solicitud.ENVIADA
        @solicitud.user_id = current_user.id
        @solicitud.email = params[:solicitud]['email']
        @solicitud.save
      else
        if @solicitud.costo_total > @solicitud.presupuesto
          redirect_to solicitud_url(@solicitud) + "#start", alert: 'Error al almacenar su solicitud: su solicitud excelede el presupuesto asignado.'
        else
          #@solicitud.en_plazo
          redirect_to solicitud_url(@solicitud) + "#start", alert: 'Error al almacenar su solicitud. El plazo para la recepcion de las solicitudes ya ha terminado.'
        end
        return
      end
    end

    Rails.logger.info("Antes de update_attributes")
    if @solicitud.update_attributes(params[:solicitud])
      Rails.logger.info("Ingresa al update_attributes")
      if params[:mode] == 'terminar'
        Rails.logger.info("modo terminar detectado")
        notice = nil
        EmailJob.new.async.perform({:solicitud_id => @solicitud.id, :type => "confirmar"})
        #Solicitud.delay.email_job({:solicitud_id => @solicitud.id, :type => "confirmar"})
        if @solicitud.periodo.tipo == 3
          notice = 'Su solicitud acaba de ser enviada y será revisada por la dirección regional. Recibirá un email cuando la solicitud sea aprobada'
        else
          notice = 'Su solicitud ha sido recibida exitósamente.'

        end
        if current_user.isAdmin? or current_user.region > 0
          redirect_to "/periodos/#{@solicitud.periodo_id}", notice: notice
        else
          reset_session

          redirect_to root_url, notice: notice
        end
      else
        redirect_to solicitud_url(@solicitud) + "#start", notice: 'Los cambios han sido guardados exitosamente'
      end
    else
      redirect_to solicitud_url(@solicitud), alert: 'Debe ingresar un correo v&aacute;lido para confirmar su solicitud.'
    end

  end

end
