require 'roo'
namespace :integra do
  desc "Carga usuarios desde xlsx"
  task :users => :environment do
    workbook = RubyXL::Parser.parse("#{Rails.root}/db/directores.xlsx")
    puts "cargando directores"
    for i in 1..workbook[0].sheet_data.size
      unless workbook[0][i].nil? or workbook[0][i][0].nil?
        usuario = Usuario.new
        usuario.username = workbook[0][i][0].value 
        usuario.password = usuario.username.to_s.slice(0,4)
        usuario.password_confirmation = usuario.username.to_s.slice(0,4)
        usuario.nombre = workbook[0][i][3].value 
	usuario.jardin_codigo = workbook[0][i][4].value 
        usuario.region = 0 
        usuario.admin = 0
        puts usuario.username
        usuario.save
      end
    end
    
    workbook = RubyXL::Parser.parse("#{Rails.root}/db/nacionales.xlsx")
    puts "cargando nacionales"
    for i in 1..workbook[0].sheet_data.size
      unless workbook[0][i].nil? or workbook[0][i][0].nil?
        usuario = Usuario.new
        usuario.username = workbook[0][i][1].value 
        usuario.password = usuario.username.to_s.slice(0,4)
        usuario.password_confirmation = usuario.username.to_s.slice(0,4)
        usuario.nombre = workbook[0][i][0].value 
        usuario.jardin_codigo = 0 
        usuario.region = 0 
        usuario.admin = 1
        puts usuario.username
        usuario.save
      end
    end
    
     workbook = RubyXL::Parser.parse("#{Rails.root}/db/regionales.xlsx")
      puts "cargando regionales"
      #hoja 1
      puts "hoja 1"
      for i in 1..workbook[0].sheet_data.size
        unless workbook[0][i].nil? or workbook[0][i][3].nil? or workbook[0][i][3].blank? or workbook[0][i][3].value.nil?
          #puts "> #{workbook[1][i][2].value}"
          #puts workbook[1][i][2].value.size 
          usuario = Usuario.new
          usuario.username = workbook[0][i][3].value 
          usuario.password = usuario.username.to_s.slice(0,4)
          usuario.password_confirmation = usuario.username.to_s.slice(0,4)
          usuario.nombre = workbook[0][i][7].value 
          usuario.jardin_codigo = 0 
          usuario.region = workbook[0][i][0].value
          usuario.admin = 0
          puts usuario.username
          usuario.save
        end
      end
      #hoja 2
      if workbook[1]
      puts "hoja 2"
      	for i in 1..workbook[1].sheet_data.size
	        unless workbook[1][i].nil? or workbook[1][i][2].nil? or workbook[1][i][2].blank? or workbook[1][i][2].value.nil?         
	          usuario = Usuario.new
	          usuario.username = workbook[1][i][2].value 
	          usuario.password = usuario.username.to_s.slice(0,4)
	          usuario.password_confirmation = usuario.username.to_s.slice(0,4)
	          usuario.nombre = workbook[1][i][6].value 
        	  usuario.jardin_codigo = 0 
	          usuario.region = workbook[1][i][0].value
        	  usuario.admin = 0
	          puts usuario.username
         	 usuario.save
        	end
      	end
      end
      #hoja 3
      if workbook[2]
      puts "hoja 3"
	for i in 1..workbook[2].sheet_data.size
    	    unless workbook[2][i].nil? or workbook[2][i][3].nil? or workbook[2][i][3].blank? or workbook[2][i][3].value.nil?           
	          usuario = Usuario.new
	          usuario.username = workbook[2][i][3].value 
	          usuario.password = usuario.username.to_s.slice(0,4)
	          usuario.password_confirmation = usuario.username.to_s.slice(0,4)
	          usuario.nombre = workbook[2][i][7].value 
	          usuario.jardin_codigo = 0 
	          usuario.region = workbook[2][i][0].value
	          usuario.admin = 0
	          puts usuario.username
	          usuario.save
             end
      	end
      end  
    
    
    
  end
end
