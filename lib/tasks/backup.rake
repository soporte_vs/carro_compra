namespace :integra do

  desc "respalda base de datos"
  task :backup => :environment do

    database = YAML.load_file("#{Rails.root}/config/database.yml")
    username = database[Rails.env]['username']
    password = database[Rails.env]['password']
    name = database[Rails.env]['database']
    host = database[Rails.env]['host']
    puts database[Rails.env]['username']

    if Rails.env.production?
      Dir.mktmpdir do |dir|
        cmd = "pg_dump --clean -h #{host} -U #{username} #{name} |bzip2 -9 > #{dir}/database.backup.sql.bz2"
        puts cmd
        system({"PGPASSWORD" => password}, cmd)
        CloudServer.upload_to_amazon('backup/database.sql.bz2', "#{dir}/database.backup.sql.bz2", false)
      end
    end

  end

end
