# -*- coding: utf-8 -*-
namespace :integra do
  desc "Genera el reporte en formato excel según los permisos del usuario que lo solicita"
  task :excel, [:userId,:periodo_id,:email]  => [:environment] do |t,params|
    puts "Argumento es #{params[:userId]} con email: #{params[:email]}"
    @periodo = Periodo.find(params[:periodo_id])
    @materiales = @periodo.materiales
    @solicitudes = nil
    u = Usuario.find(params[:userId].to_i)
    if (u.isAdmin?)
      @solicitudes = @periodo.solicitudes
    else
    	if (u.region > 0)
    		@solicitudes = Solicitud.find(:all, :include => [:jardin],:conditions => ["periodo_id = #{@periodo.id} and jardines.region = ?",u.region], :order => "jardin_codigo ASC")
    	else
    		@solicitudes = Solicitud.find(:all, :include => [:jardin],:conditions => ["periodo_id = #{@periodo.id} and jardines.codigo = ?",u.jardin_codigo], :order => "jardin_codigo ASC")
    	end
    end
    

    av = ActionView::Base.new(Integra::Application.config.paths["app/views"].first)
    xls = av.render(:partial => "solicitudes/xls", :format => 'xml', :locals => {:materiales => @materiales, :solicitudes => @solicitudes})
    
    Notifier.reporte(params[:email],params[:userId].to_i,xls).deliver
    puts "enviado"
    
  end
end