# -*- coding: utf-8 -*-
namespace :integra do
  desc "Elimina detalle duplicado en base de dato"
  task :clean => :environment do

    materiales = Material.find(:all, :conditions => ["periodo_id = 2"])
    output = []
    for s in Solicitud.find(:all, :conditions => ["periodo_id = 2"])

      for m in materiales
        dd = Detalle.find(:all, :conditions => ["solicitud_id = ? and material_id = ?",s.id,m.id], :order => "updated_at DESC")

        if dd.size > 1
          da = dd.to_a
          for i in 1..(da.size-1)
            target = da[i]
            output.push s.id
            puts "problema #{s.id} #{m.id} #{da.size}"
            puts "destroy #{target.id} - solicitada #{target.cantidad_solicitada}"
            Detalle.destroy(target.id)
          end
        #else
        #  puts "no problema #{s.id} #{m.id}"
        end
      end
    end
    puts output


  end


end