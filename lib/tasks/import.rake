# -*- coding: utf-8 -*-
require 'roo'
namespace :integra do

  desc 'test email'
  task :test_email => :environment do
    Notifier.test_email.deliver
  end

  desc 'Procesa archivo con usuarios regionales'
  task :cargar_usuarios_regionales => :environment do
    Rails.application.eager_load!
    input = Roo::Excelx.new("#{Rails.root}/db/regionales.xlsx")
    for u in Usuario.where("region > 0")
      u.valido = false
      u.save!
    end
    for i in 2..input.last_row
      region = input.cell(i,'A').to_i
      begin
        username = input.cell(i,'D').downcase
      rescue
        username = input.cell(i,'D').to_i.to_s
      end
      nombre = input.cell(i,'H')

      usuario = Usuario.where(:username => username).first
      if usuario.nil?
        puts "creando #{username}"
        usuario = Usuario.new
        usuario.username = username
        usuario.password = usuario.username[0,4]
      else
        puts "actualizando #{username}"
      end
      usuario.email = input.cell(i,'L')
      usuario.nombre = nombre
      usuario.region = region
      usuario.valido = true
      usuario.save!
    end

  end

  desc 'Procesa archivo con usuarios'
  task :procesar_usuarios => :environment do
    Rails.application.eager_load!
    #UsuarioJob.new.async.perform()
    Usuario.delay.usuario_job
  end


  desc "procesar presupuestos"
  task :presupuesto, [:periodo_id] => [:environment] do |t,args|
    Rails.application.eager_load!
    PresupuestoJob.new.perform(args[:periodo_id])


  end


  desc "Procesa archivo zip de materiales con imágenes"
  task :imagenes_materiales, [:periodo_id] => [:environment] do |t,args|
    Rails.application.eager_load!
    ImagenJob.new.perform(args[:periodo_id])

  end

  desc "Carga equipamiento de continuidad"
  task :continuidad => :environment do
    def buscaCategoria(workbook,col)
      aux = workbook[0][2][col]
      if (aux != nil && aux.value != nil && aux.value.length > 1)
        return aux.value.strip
      else
        return buscaCategoria(workbook,col-1)
      end
    end

    def buscaNiveles(workbook,i,col=4)
      output = []
      for j in col..(col+9)
        nivel = workbook[0][1][j].value
        val = workbook[0][i][j].value
        if val.to_i > 0
          output << nivel
        end
      end
      return output
    end



    Rails.cache.clear

    puts "actualizamos presupuestos"
    puts "eliminamos solicitudes actuales"
    solicitudes = Solicitud.find(:all, :conditions => ["periodo_id = #{PERIODO}"])
    for s in solicitudes
      s.destroy
    end
    for j in Jardin.find(:all, :conditions => "activo = 1")
      j.activo = 0
      j.save
    end

    puts "cargamos presupuestos y niveles desde xlsx"
    workbook = RubyXL::Parser.parse("#{Rails.root}/db/jardines-equipamiento-2012.xlsx")
    for i in 2..workbook[0].sheet_data.size
      row = workbook[0][i]
      unless row.nil? or row[2].nil?
        codigo = row[2].value
        j = Jardin.find(:first, :conditions => ["codigo = ?",codigo])
        if j.nil?
          puts "Jardin #{codigo} no existe!"
          j = Jardin.new
          j.codigo = codigo
          j.region = row[0].value
          j.comuna = row[1].value
          j.nombre = row[2].value
          j.modalidad_atencion = "JARDÍN INFANTIL"
          j.save!
        end
        puts "#{i+1} -> jardin: #{codigo}"
        #puts "#{row[14].value}"
        j.activo = 1
        j.save!

	Presupuesto.create presupuesto: row[14].value, jardin_id: j.id, periodo_id: PERIODO # j.presupuesto = row[14].value

        #puts "eliminando los niveles anteriores"
        jn = JardinNivel.find(:all, :conditions => ["jardin_codigo = ?",codigo])
        for a in jn
          a.destroy
        end

        #puts "cargando nuevos niveles"
        for nivel in buscaNiveles(workbook,i)
          jn = JardinNivel.new
          jn.jardin_codigo = j.codigo
          jn.nivel = nivel
          jn.save!
        end
      end
    end

    puts "eliminando equipamiento de continuidad"
    Material.connection.execute("DELETE FROM materiales where periodo_id = #{PERIODO}")
    # materiales = Material.find(:all, :conditions => ["periodo_id = ?",PERIODO])
    # for m in materiales
    #   m.destroy
    # end

    workbook = RubyXL::Parser.parse("#{Rails.root}/db/productos-equipamiento-2012.xlsx")
    puts "cargando equipamiento de continuidad"
    for i in 2..workbook[0].sheet_data.size
      row = workbook[0][i]
      unless row.nil? or row[2].nil?
        categoria = row[0].value.strip
        codigo = row[1].value.strip.gsub(/\s/,'')
        nombre = row[2].value.strip
        caracteristicas = row[3].value.strip
        coeficiente_tecnico = row[4].value.strip
        precio = row[15].value
        #puts precio
        c = Categoria.find(:first, :conditions => ["nombre = ?",categoria])
        if c.nil?
          c = Categoria.new
          c.nombre = categoria
          c.save!
        end
        m = Material.find(:first, :conditions => ["periodo_id = ? and codigo = ?",PERIODO,codigo])
        if m.nil?
          m = Material.new
          m.codigo = codigo
          m.periodo_id = PERIODO
        end

        m.producto = nombre
        m.precio_neto = precio
        m.caracteristicas = caracteristicas
        m.coeficienteTecnico =  coeficiente_tecnico
        m.categoria_id = c.id
        m.save!



        niveles = buscaNiveles(workbook,i,5)
        for nivel in niveles
          mn = MaterialNivel.new
          mn.material_codigo = m.codigo
          mn.nivel = nivel
          mn.save!
        end

      end
    end



  end

  desc "Carga datos desde xlsx"
  task :import => :environment do
    #gem install goo
    Rails.cache.clear
    faltantes = []
    workbook = RubyXL::Parser.parse("#{Rails.root}/db/jardines2012.xlsx")
    puts "cargando jardines"
    for i in 1..workbook[0].sheet_data.size
      jardin = Jardin.new
      unless workbook[0][i].nil? or workbook[0][i][0].nil?
        jardin.region = workbook[0][i][0].value
        jardin.comuna = workbook[0][i][1].value
        jardin.codigo = workbook[0][i][2].value
        jardin.nombre = workbook[0][i][3].value
        jardin.modalidad_atencion = workbook[0][i][4].value
        puts jardin.codigo if jardin.save
      end
    end

    puts "cargando fungibles"
    workbook = RubyXL::Parser.parse("#{Rails.root}/db/Fungible 2012.xlsx")

    MAX_LINE = workbook[1].sheet_data.size-3
    MIN_COL = 33
    MAX_COL = 62

    periodo = Periodo.find(:first, :conditions => ["id = ?",PERIODO])
    if periodo.nil?
      periodo = Periodo.new
      periodo.activo = 1
      periodo.id = PERIODO
      periodo.save!
    end

    for i in MIN_COL..MAX_COL
      puts i
      producto = workbook[1][2][i].value
      codigo = workbook[1][1][i].value
      precio_neto = workbook[1][0][i].value
      material = Material.find(:first, :conditions => ["codigo = ? AND periodo_id = ?",codigo,periodo])
      if material.nil?
        material = Material.new
        material.codigo = codigo
        material.producto = producto
        material.periodo_id = PERIODO
      end
      material.precio_neto = precio_neto
      puts producto
      material.save
    end

    for i in 3..MAX_LINE
      puts "buscando jardin by codigo #{workbook[1][i][2].value}"
      jardin = Jardin.find_by_codigo(workbook[1][i][2].value)
      if jardin.nil?
        faltantes << workbook[1][i][2].value
        puts "ERROR: no se encuentra jardin: #{workbook[1][i][2].value}"
      else
        solicitud = Solicitud.find(:first, :conditions => ["jardin_codigo = ? and periodo_id = ?",jardin.codigo,PERIODO]) # jardin_id --> jardin_codigo
        if solicitud.nil?
          solicitud = Solicitud.new
          solicitud.jardin_codigo = jardin.codigo # jardin_id --> jardin_codigo
          solicitud.periodo_id = PERIODO
          solicitud.estado = 0
          solicitud.save!
        end
        presupuesto = 0
        for j in MIN_COL..MAX_COL
          codigo = workbook[1][1][j].value
          m = Material.find_by_codigo(codigo)
          detalle = Detalle.new
          detalle.material_id = m.id
          detalle.cantidad_propuesta = workbook[1][i][j].value
          detalle.cantidad_solicitada = workbook[1][i][j].value
          detalle.solicitud_id = solicitud.id
          #puts codigo
          #puts detalle.material_id
          #puts detalle.cantidad_propuesta
          detalle.save!
          presupuesto = presupuesto + (1.0*detalle.cantidad_propuesta * detalle.material.precio_neto)
        end
        if solicitud
		solicitud.presupuesto = presupuesto.round
	        solicitud.save
	end

        #puts workbook[1][i][3].value
      end
    end

    puts "los siguientes jardines estan en la lista de fungibles pero no en la base de datos de jardines:"
    for i in faltantes
      puts i
    end


  end


end
