# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20190222200352) do

  create_table "carros", :force => true do |t|
    t.string   "usuarios"
    t.integer  "usuarios_status"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "categorias", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "color",      :default => "rgb(255,255,255)", :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "detalles", :force => true do |t|
    t.integer  "solicitud_id"
    t.integer  "material_id"
    t.integer  "cantidad_propuesta",     :default => 0, :null => false
    t.integer  "cantidad_solicitada",    :default => 0, :null => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "cantidad_actual",        :default => 0
    t.integer  "cantidad_a_dar_de_baja", :default => 0
    t.integer  "cantidad_con_deterioro", :default => 0
    t.integer  "cantidad_scme",          :default => 0
    t.integer  "cantidad_scma",          :default => 0
    t.integer  "cantidad_sc",            :default => 0
    t.integer  "cantidad_mme",           :default => 0
    t.integer  "cantidad_mma",           :default => 0
    t.integer  "cantidad_med",           :default => 0
    t.integer  "cantidad_tme",           :default => 0
    t.integer  "cantidad_tma",           :default => 0
    t.integer  "cantidad_tra",           :default => 0
    t.integer  "cantidad_het",           :default => 0
    t.integer  "cantidad_ji",            :default => 0
  end

  create_table "jardines", :force => true do |t|
    t.integer  "codigo"
    t.integer  "region"
    t.string   "comuna"
    t.string   "nombre"
    t.string   "modalidad_atencion"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "activo",             :default => 0, :null => false
  end

  create_table "materiales", :force => true do |t|
    t.string   "licitacion"
    t.string   "codigo"
    t.string   "producto"
    t.float    "precio_neto"
    t.integer  "periodo_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.text     "caracteristicas"
    t.text     "coeficienteTecnico"
    t.integer  "categoria_id",       :default => 0
    t.string   "image"
    t.boolean  "has_picture",        :default => false, :null => false
    t.string   "unidad"
  end

  create_table "nivel_jardin", :force => true do |t|
    t.integer "jardin_codigo"
    t.string  "nivel"
  end

  create_table "nivel_material", :force => true do |t|
    t.string "nivel"
    t.string "material_codigo"
  end

  create_table "niveles", :force => true do |t|
    t.string "codigo"
  end

  create_table "periodos", :force => true do |t|
    t.integer  "status",           :default => 0,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    :null => false
    t.text     "instrucciones",    :default => "<p>Por favor ajuste las cantidades que va a solicitar de cada producto en la\ncolumna Cantidad solicitada, de acuerdo a su necesidad de equipamiento en\nel establecimiento. Recuerde que no puede exceder con su pedido el valor\nque tiene asignado como presupuesto.</p>\n<p>Puede guardar los cambios en la planilla si así lo desea utilizando el\nbotón celeste \"Guardar los cambios\", que se encuentra al final de la\npágina , pero una vez finalizado el pedido debe presionar el botón verde \"Terminar el pedido\",  y luego se le solicitará su correo electrónico al cual le será enviado un comprobante, una vez validado en la Dirección\nRegional. Las fotografías de los productos son sólo referenciales, basadas\nen compras recientes.</p>\n", :null => false
    t.datetime "created_at",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         :null => false
    t.datetime "updated_at",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         :null => false
    t.integer  "modo",             :default => 0
    t.datetime "time_start",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         :null => false
    t.datetime "time_end",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           :null => false
    t.string   "nombre"
    t.string   "planilla"
    t.string   "planilla_tmp"
    t.string   "batch_url"
    t.string   "batch"
    t.string   "batch_tmp"
    t.boolean  "batch_procesando", :default => false,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                :null => false
    t.text     "aprobado",         :default => "Le informamos que su solicitud luego de ser revisada por la oficina regional, ha sido <b>APROBADA</b>,\npor lo que será transmitida al nivel central para que gestione la adquisición.\nLe recordamos que el equipamiento será despachado a los establecimientos entre los meses de\nAbril y Mayo del 2013. Muchas gracias por hacer uso de la plataforma para gestionar su pedido\n",                                                                                                                                                                                                                                                                                                                                                                               :null => false
    t.integer  "tipo",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               :null => false
  end

  create_table "presupuestos", :force => true do |t|
    t.integer  "jardin_id",                  :null => false
    t.integer  "periodo_id",                 :null => false
    t.integer  "presupuesto", :default => 0, :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "redactor_assets", :force => true do |t|
    t.integer  "user_id"
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], :name => "idx_redactor_assetable"
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_redactor_assetable_type"

  create_table "sessiones", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessiones", ["session_id"], :name => "index_sessiones_on_session_id"
  add_index "sessiones", ["updated_at"], :name => "index_sessiones_on_updated_at"

  create_table "solicitudes", :force => true do |t|
    t.integer  "jardin_codigo"
    t.integer  "periodo_id"
    t.integer  "estado",                                 :default => 0,        :null => false
    t.datetime "created_at",                                                   :null => false
    t.datetime "updated_at",                                                   :null => false
    t.integer  "presupuesto"
    t.string   "email"
    t.integer  "user_id"
    t.string   "estado_nombre",                          :default => "CREADA", :null => false
    t.text     "observacion_solicitante"
    t.string   "observacion_administrador"
    t.integer  "costo_total",               :limit => 8, :default => 0,        :null => false
  end

  add_index "solicitudes", ["jardin_codigo", "periodo_id"], :name => "index_solicitudes_on_jardin_codigo_and_periodo_id", :unique => true

  create_table "usuarios", :force => true do |t|
    t.string   "username"
    t.string   "password_digest"
    t.string   "nombre"
    t.integer  "jardin_codigo",   :default => 0
    t.integer  "region",          :default => 0
    t.integer  "admin",           :default => 0
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.boolean  "valido",          :default => true,  :null => false
    t.string   "email"
    t.boolean  "autoriza",        :default => false, :null => false
  end

  add_index "usuarios", ["username"], :name => "index_usuarios_on_username", :unique => true

end
