--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: carros; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE carros (
    id integer NOT NULL,
    usuarios character varying(255),
    usuarios_status integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: carros_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE carros_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: carros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE carros_id_seq OWNED BY carros.id;


--
-- Name: categorias; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE categorias (
    id integer NOT NULL,
    nombre character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    color character varying(255) DEFAULT 'rgb(255,255,255)'::character varying NOT NULL
);


--
-- Name: categorias_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categorias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categorias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categorias_id_seq OWNED BY categorias.id;


--
-- Name: detalles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE detalles (
    id integer NOT NULL,
    solicitud_id integer,
    material_id integer,
    cantidad_propuesta integer DEFAULT 0 NOT NULL,
    cantidad_solicitada integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    cantidad_actual integer DEFAULT 0,
    cantidad_a_dar_de_baja integer DEFAULT 0,
    cantidad_con_deterioro integer DEFAULT 0
);


--
-- Name: detalles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE detalles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: detalles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE detalles_id_seq OWNED BY detalles.id;


--
-- Name: jardines; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE jardines (
    id integer NOT NULL,
    codigo integer,
    region integer,
    comuna character varying(255),
    nombre character varying(255),
    modalidad_atencion character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    activo integer DEFAULT 0 NOT NULL
);


--
-- Name: jardines_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE jardines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: jardines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE jardines_id_seq OWNED BY jardines.id;


--
-- Name: materiales; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE materiales (
    id integer NOT NULL,
    licitacion character varying(255),
    codigo character varying(255),
    producto character varying(255),
    precio_neto double precision,
    periodo_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    caracteristicas text,
    "coeficienteTecnico" text,
    categoria_id integer DEFAULT 0,
    image character varying(255),
    has_picture boolean DEFAULT false NOT NULL,
    unidad character varying(255)
);


--
-- Name: materiales_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE materiales_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: materiales_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE materiales_id_seq OWNED BY materiales.id;


--
-- Name: nivel_jardin; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nivel_jardin (
    id integer NOT NULL,
    jardin_codigo integer,
    nivel character varying(255)
);


--
-- Name: nivel_jardin_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE nivel_jardin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: nivel_jardin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE nivel_jardin_id_seq OWNED BY nivel_jardin.id;


--
-- Name: nivel_material; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nivel_material (
    id integer NOT NULL,
    nivel character varying(255),
    material_codigo character varying(255)
);


--
-- Name: nivel_material_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE nivel_material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: nivel_material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE nivel_material_id_seq OWNED BY nivel_material.id;


--
-- Name: niveles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE niveles (
    id integer NOT NULL,
    codigo character varying(255)
);


--
-- Name: niveles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE niveles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: niveles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE niveles_id_seq OWNED BY niveles.id;


--
-- Name: periodos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE periodos (
    id integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    instrucciones text DEFAULT '<p>Por favor ajuste las cantidades que va a solicitar de cada producto en la
columna Cantidad solicitada, de acuerdo a su necesidad de equipamiento en
el establecimiento. Recuerde que no puede exceder con su pedido el valor
que tiene asignado como presupuesto.</p>
<p>Puede guardar los cambios en la planilla si así lo desea utilizando el
botón celeste "Guardar los cambios", que se encuentra al final de la
página , pero una vez finalizado el pedido debe presionar el botón verde "Terminar el pedido",  y luego se le solicitará su correo electrónico al cual le será enviado un comprobante, una vez validado en la Dirección
Regional. Las fotografías de los productos son sólo referenciales, basadas
en compras recientes.</p>
'::text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    modo integer DEFAULT 0,
    time_start timestamp without time zone NOT NULL,
    time_end timestamp without time zone NOT NULL,
    nombre character varying(255),
    planilla character varying(255),
    planilla_tmp character varying(255),
    batch_url character varying(255),
    batch character varying(255),
    batch_tmp character varying(255),
    batch_procesando boolean DEFAULT false NOT NULL,
    aprobado text DEFAULT 'Le informamos que su solicitud luego de ser revisada por la oficina regional, ha sido <b>APROBADA</b>,
por lo que será transmitida al nivel central para que gestione la adquisición.
Le recordamos que el equipamiento será despachado a los establecimientos entre los meses de
Abril y Mayo del 2013. Muchas gracias por hacer uso de la plataforma para gestionar su pedido
'::text NOT NULL,
    tipo integer NOT NULL
);


--
-- Name: periodos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE periodos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: periodos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE periodos_id_seq OWNED BY periodos.id;


--
-- Name: presupuestos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE presupuestos (
    id integer NOT NULL,
    jardin_id integer NOT NULL,
    periodo_id integer NOT NULL,
    presupuesto integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: presupuestos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE presupuestos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: presupuestos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE presupuestos_id_seq OWNED BY presupuestos.id;


--
-- Name: redactor_assets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE redactor_assets (
    id integer NOT NULL,
    user_id integer,
    data_file_name character varying(255) NOT NULL,
    data_content_type character varying(255),
    data_file_size integer,
    assetable_id integer,
    assetable_type character varying(30),
    type character varying(30),
    width integer,
    height integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: redactor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE redactor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: redactor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE redactor_assets_id_seq OWNED BY redactor_assets.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: sessiones; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sessiones (
    id integer NOT NULL,
    session_id character varying(255) NOT NULL,
    data text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: sessiones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sessiones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessiones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sessiones_id_seq OWNED BY sessiones.id;


--
-- Name: solicitudes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE solicitudes (
    id integer NOT NULL,
    jardin_codigo integer,
    periodo_id integer,
    estado integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    presupuesto integer,
    email character varying(255),
    user_id integer,
    estado_nombre character varying(255) DEFAULT 'CREADA'::character varying NOT NULL,
    observacion_solicitante text,
    observacion_administrador character varying(255),
    costo_total bigint DEFAULT 0 NOT NULL
);


--
-- Name: solicitudes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE solicitudes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solicitudes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE solicitudes_id_seq OWNED BY solicitudes.id;


--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE usuarios (
    id integer NOT NULL,
    username character varying(255),
    password_digest character varying(255),
    nombre character varying(255),
    jardin_codigo integer DEFAULT 0,
    region integer DEFAULT 0,
    admin integer DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    valido boolean DEFAULT true NOT NULL,
    email character varying(255),
    autoriza boolean DEFAULT false NOT NULL
);


--
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE usuarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE usuarios_id_seq OWNED BY usuarios.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY carros ALTER COLUMN id SET DEFAULT nextval('carros_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categorias ALTER COLUMN id SET DEFAULT nextval('categorias_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY detalles ALTER COLUMN id SET DEFAULT nextval('detalles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY jardines ALTER COLUMN id SET DEFAULT nextval('jardines_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY materiales ALTER COLUMN id SET DEFAULT nextval('materiales_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY nivel_jardin ALTER COLUMN id SET DEFAULT nextval('nivel_jardin_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY nivel_material ALTER COLUMN id SET DEFAULT nextval('nivel_material_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY niveles ALTER COLUMN id SET DEFAULT nextval('niveles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY periodos ALTER COLUMN id SET DEFAULT nextval('periodos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY presupuestos ALTER COLUMN id SET DEFAULT nextval('presupuestos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY redactor_assets ALTER COLUMN id SET DEFAULT nextval('redactor_assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessiones ALTER COLUMN id SET DEFAULT nextval('sessiones_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY solicitudes ALTER COLUMN id SET DEFAULT nextval('solicitudes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY usuarios ALTER COLUMN id SET DEFAULT nextval('usuarios_id_seq'::regclass);


--
-- Name: carros_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY carros
    ADD CONSTRAINT carros_pkey PRIMARY KEY (id);


--
-- Name: categorias_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY categorias
    ADD CONSTRAINT categorias_pkey PRIMARY KEY (id);


--
-- Name: detalles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY detalles
    ADD CONSTRAINT detalles_pkey PRIMARY KEY (id);


--
-- Name: jardines_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY jardines
    ADD CONSTRAINT jardines_pkey PRIMARY KEY (id);


--
-- Name: materiales_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY materiales
    ADD CONSTRAINT materiales_pkey PRIMARY KEY (id);


--
-- Name: nivel_jardin_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nivel_jardin
    ADD CONSTRAINT nivel_jardin_pkey PRIMARY KEY (id);


--
-- Name: nivel_material_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nivel_material
    ADD CONSTRAINT nivel_material_pkey PRIMARY KEY (id);


--
-- Name: niveles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY niveles
    ADD CONSTRAINT niveles_pkey PRIMARY KEY (id);


--
-- Name: periodos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY periodos
    ADD CONSTRAINT periodos_pkey PRIMARY KEY (id);


--
-- Name: presupuestos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY presupuestos
    ADD CONSTRAINT presupuestos_pkey PRIMARY KEY (id);


--
-- Name: redactor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY redactor_assets
    ADD CONSTRAINT redactor_assets_pkey PRIMARY KEY (id);


--
-- Name: sessiones_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sessiones
    ADD CONSTRAINT sessiones_pkey PRIMARY KEY (id);


--
-- Name: solicitudes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY solicitudes
    ADD CONSTRAINT solicitudes_pkey PRIMARY KEY (id);


--
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- Name: idx_redactor_assetable; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_redactor_assetable ON redactor_assets USING btree (assetable_type, assetable_id);


--
-- Name: idx_redactor_assetable_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_redactor_assetable_type ON redactor_assets USING btree (assetable_type, type, assetable_id);


--
-- Name: index_sessiones_on_session_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_sessiones_on_session_id ON sessiones USING btree (session_id);


--
-- Name: index_sessiones_on_updated_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_sessiones_on_updated_at ON sessiones USING btree (updated_at);


--
-- Name: index_solicitudes_on_jardin_codigo_and_periodo_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_solicitudes_on_jardin_codigo_and_periodo_id ON solicitudes USING btree (jardin_codigo, periodo_id);


--
-- Name: index_usuarios_on_username; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_usuarios_on_username ON usuarios USING btree (username);


--
-- Name: una_solicitud_un_material; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX una_solicitud_un_material ON detalles USING btree (solicitud_id, material_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20120515031419');

INSERT INTO schema_migrations (version) VALUES ('20120515032944');

INSERT INTO schema_migrations (version) VALUES ('20120515033159');

INSERT INTO schema_migrations (version) VALUES ('20120516022003');

INSERT INTO schema_migrations (version) VALUES ('20120516022056');

INSERT INTO schema_migrations (version) VALUES ('20120517133046');

INSERT INTO schema_migrations (version) VALUES ('20120521173715');

INSERT INTO schema_migrations (version) VALUES ('20120524191913');

INSERT INTO schema_migrations (version) VALUES ('20120613170240');

INSERT INTO schema_migrations (version) VALUES ('20120616000933');

INSERT INTO schema_migrations (version) VALUES ('20120804030432');

INSERT INTO schema_migrations (version) VALUES ('20120805232039');

INSERT INTO schema_migrations (version) VALUES ('20120806003500');

INSERT INTO schema_migrations (version) VALUES ('20120807232536');

INSERT INTO schema_migrations (version) VALUES ('20120808021052');

INSERT INTO schema_migrations (version) VALUES ('20120810120236');

INSERT INTO schema_migrations (version) VALUES ('20120811175457');

INSERT INTO schema_migrations (version) VALUES ('20120811193021');

INSERT INTO schema_migrations (version) VALUES ('20120818130810');

INSERT INTO schema_migrations (version) VALUES ('20120818134255');

INSERT INTO schema_migrations (version) VALUES ('20120818183440');

INSERT INTO schema_migrations (version) VALUES ('20120818212711');

INSERT INTO schema_migrations (version) VALUES ('20120826154859');

INSERT INTO schema_migrations (version) VALUES ('20120827120131');

INSERT INTO schema_migrations (version) VALUES ('20120831114212');

INSERT INTO schema_migrations (version) VALUES ('20120902210916');

INSERT INTO schema_migrations (version) VALUES ('20120926134956');

INSERT INTO schema_migrations (version) VALUES ('20130414184137');

INSERT INTO schema_migrations (version) VALUES ('20130414225603');

INSERT INTO schema_migrations (version) VALUES ('20130414230343');

INSERT INTO schema_migrations (version) VALUES ('20130415221651');

INSERT INTO schema_migrations (version) VALUES ('20130418235645');

INSERT INTO schema_migrations (version) VALUES ('20130420152424');

INSERT INTO schema_migrations (version) VALUES ('20130420193751');

INSERT INTO schema_migrations (version) VALUES ('20130504145624');

INSERT INTO schema_migrations (version) VALUES ('20130504214917');

INSERT INTO schema_migrations (version) VALUES ('20130505145801');

INSERT INTO schema_migrations (version) VALUES ('20130505160648');

INSERT INTO schema_migrations (version) VALUES ('20130505200039');

INSERT INTO schema_migrations (version) VALUES ('20130507023605');

INSERT INTO schema_migrations (version) VALUES ('20130514223427');

INSERT INTO schema_migrations (version) VALUES ('20130514233749');

INSERT INTO schema_migrations (version) VALUES ('20130515000638');

INSERT INTO schema_migrations (version) VALUES ('20130515004600');

INSERT INTO schema_migrations (version) VALUES ('20130516015940');

INSERT INTO schema_migrations (version) VALUES ('20130516232011');

INSERT INTO schema_migrations (version) VALUES ('20130516233020');

INSERT INTO schema_migrations (version) VALUES ('20130518152319');

INSERT INTO schema_migrations (version) VALUES ('20130519190329');

INSERT INTO schema_migrations (version) VALUES ('20130519214814');

INSERT INTO schema_migrations (version) VALUES ('20130522220307');

INSERT INTO schema_migrations (version) VALUES ('20130604014731');

INSERT INTO schema_migrations (version) VALUES ('20130605002340');

INSERT INTO schema_migrations (version) VALUES ('20130606232534');

INSERT INTO schema_migrations (version) VALUES ('20130710151318');

INSERT INTO schema_migrations (version) VALUES ('20130805183232');

INSERT INTO schema_migrations (version) VALUES ('20130908193336');

INSERT INTO schema_migrations (version) VALUES ('20140524155950');

INSERT INTO schema_migrations (version) VALUES ('20140528153026');

INSERT INTO schema_migrations (version) VALUES ('20140604202417');

INSERT INTO schema_migrations (version) VALUES ('20150926140340');