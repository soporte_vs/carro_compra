class DefaultColor < ActiveRecord::Migration
  def up
    remove_column :categorias, :color
    add_column :categorias, :color, :string, :default => "rgb(255,255,255)", :null => false
  end

  def down
  end
end
