class SolicitudesUnique < ActiveRecord::Migration
  def up
    add_index :solicitudes, [:jardin_codigo, :periodo_id], :unique => true
  end

  def down
  end
end
