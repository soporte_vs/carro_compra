class CreateCategorias < ActiveRecord::Migration
  def change
    create_table :categorias do |t|
      t.string :nombre
      t.string :color
      t.timestamps
    end
    add_column :materiales, :categoria_id, :integer, :default => 0
  end
end
