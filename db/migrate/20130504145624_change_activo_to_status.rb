class ChangeActivoToStatus < ActiveRecord::Migration
  def up
    rename_column :periodos, :activo, :status
    Periodo.connection.execute("UPDATE periodos set status = 6")
  end

  def down
  end
end
