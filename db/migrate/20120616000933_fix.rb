class Fix < ActiveRecord::Migration
  def up
    jardines = [102302,103302,11104]
    for j in jardines
      s = Solicitud.find_by_jardin_id(j)
      if s 
	s.estado = 0
      	s.save!
      	Rails.cache.delete("views/estado-solicitud-#{s.id}")
      end
    end
  end

  def down
  end
end
