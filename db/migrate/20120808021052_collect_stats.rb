class CollectStats < ActiveRecord::Migration
  def up
    add_column :detalles, :cantidad_actual, :integer, :default => 0
    add_column :detalles, :cantidad_a_dar_de_baja, :integer, :default => 0
    add_column :detalles, :cantidad_con_deterioro, :integer, :default => 0
  end

  def down
    remove_column :detalles, :cantidad_actual
    remove_column :detalles, :cantidad_a_dar_de_baja
    remove_column :detalles, :cantidad_con_deterioro
  end
end
