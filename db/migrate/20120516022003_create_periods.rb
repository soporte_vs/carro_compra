class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periodos do |t|
      t.integer :activo
      t.text :comentarios
      t.timestamps
    end
  end
end
