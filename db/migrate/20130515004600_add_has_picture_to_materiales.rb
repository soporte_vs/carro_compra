class AddHasPictureToMateriales < ActiveRecord::Migration
  def change
    add_column :materiales, :has_picture, :boolean, :default => false, :null => false
  end
end
