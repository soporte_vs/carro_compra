class AddUniqueOnUsername < ActiveRecord::Migration
  def up
    add_index :usuarios, :username, :unique => true
  end

  def down
  end
end
