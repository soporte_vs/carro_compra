class ResetCarroStatus < ActiveRecord::Migration
  def up
    @carro = Carro.first
    if @carro.usuarios_status == 1
      @carro.usuarios_status = 2
    end
    @carro.save!

  end

  def down
  end
end
