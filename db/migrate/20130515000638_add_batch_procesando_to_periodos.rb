class AddBatchProcesandoToPeriodos < ActiveRecord::Migration
  def change
    add_column :periodos, :batch_procesando, :boolean, :null => false, :default => false
  end
end
