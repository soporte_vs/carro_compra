class ChangeActivoFromPeriodos < ActiveRecord::Migration
  def up
    change_column :periodos, :activo, :integer, :null => false, :default => 0
    change_column :periodos, :time_start, :timestamp, :null => false, :default => Time.now
    change_column :periodos, :time_end, :timestamp, :null => false, :default => Time.now

  end

  def down
  end
end
