class CreateMateriales < ActiveRecord::Migration
  def change
    create_table :materiales do |t|
      t.string :licitacion
      t.string :codigo
      t.string :producto
      t.float :precio_neto
      t.integer :periodo_id
      t.timestamps
    end
  end
end
