class Bigint < ActiveRecord::Migration
  def up
    change_column :solicitudes, :costo_total, :bigint, :null => false, :default => 0
  end

  def down
  end
end
