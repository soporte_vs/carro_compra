class Niveles2 < ActiveRecord::Migration
  def up
    drop_table :niveles
    drop_table :nivel_jardin
    drop_table :nivel_material
    
    create_table :niveles do |d|
      d.string "codigo"
    end 
    
    create_table :nivel_jardin do |d|
      d.integer "jardin_codigo"
      d.string "nivel"
    end
    
    create_table :nivel_material do |d|
      d.string "nivel"
      d.string "material_codigo"
    end
    
  end

  def down
  end
end
