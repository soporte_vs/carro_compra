class AddValidoToUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :valido, :boolean, :null => false, :default => true
  end
end
