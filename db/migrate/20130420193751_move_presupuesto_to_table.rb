class MovePresupuestoToTable < ActiveRecord::Migration
  def up
    for s in Solicitud.order("periodo_id ASC").all
      puts "Periodo: #{s.periodo_id}\tSolicitud: #{s.id}\tCosto Total: #{s.costo_total}"

      p = Presupuesto.where(['jardin_id = ? and periodo_id = ?',s.jardin.id,s.periodo.id]).first
      if p.nil?
        p = Presupuesto.new
        p.jardin_id = s.jardin.id
        p.periodo_id = s.periodo_id
      end
      p.presupuesto= s.presupuesto
      p.save!

    end
  end

  def down
  end
end
