class AddTotalSolicitadoToSolicitudes < ActiveRecord::Migration
  def change
    add_column :solicitudes, :costo_total, :integer, :null => false, :default => 0

    for s in Solicitud.order("periodo_id ASC").all
      s.calcular_total_solicitado
      s.save
      puts "Periodo: #{s.periodo_id}\tSolicitud: #{s.id}\tCosto Total: #{s.costo_total}"
    end


  end
end
