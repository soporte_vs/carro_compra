class CreateNiveles < ActiveRecord::Migration
  def up
    create_table :niveles do |d|
      d.string "codigo"
    end 
    
    create_table :nivel_jardin do |d|
      d.integer "jardin_id"
      d.integer "nivel_id"
    end
    
    create_table :nivel_material do |d|
      d.integer "nivel_id"
      d.integer "material_id"
    end
    
  end

  def down
  end
end
