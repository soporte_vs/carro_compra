class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string 'username'
      t.string 'password_digest'
      t.string 'nombre'
      t.integer 'jardin_id', :default => 0
      t.integer 'region', :default => 0
      t.integer 'admin', :default => 0
      t.timestamps
    end
  end
end
