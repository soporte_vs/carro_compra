class PeriodoMigration < ActiveRecord::Migration
  def up
    for p in Periodo.find(:all)
      p.activo = false
    end
    p = Periodo.new
    p.activo = true
    p.id = PERIODO
    p.save!
  end

  def down
  end
end
