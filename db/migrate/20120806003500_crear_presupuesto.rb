class CrearPresupuesto < ActiveRecord::Migration
  def up
    add_column :jardines, :presupuesto, :integer, :default => 0
  end

  def down
    remove_column :jardines, :presupuesto
  end
end
