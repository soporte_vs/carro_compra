class AddActivoToJardines < ActiveRecord::Migration
  def up
    add_column "jardines", "activo", :integer, :null => false, :default => 0
  end
end
