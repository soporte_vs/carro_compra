class CreateJardins < ActiveRecord::Migration
  def change
    create_table :jardines do |t|
      t.integer :codigo
      t.integer :region
      t.string :comuna
      t.string :nombre
      t.string :modalidad_atencion
      t.timestamps
    end
  end
end
