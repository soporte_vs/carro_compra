class AddNombreToPeriodo < ActiveRecord::Migration
  def up
    add_column :periodos, :nombre, :string
  end

  def down
    remove_column :periodos, :nombre
  end



end
