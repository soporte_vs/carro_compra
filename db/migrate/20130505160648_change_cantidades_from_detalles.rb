class ChangeCantidadesFromDetalles < ActiveRecord::Migration
  def up
    change_column :detalles, :cantidad_propuesta, :integer, :null => false, :default => 0
    change_column :detalles, :cantidad_solicitada, :integer, :null => false, :default => 0
  end

  def down
  end
end
