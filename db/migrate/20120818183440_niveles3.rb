class Niveles3 < ActiveRecord::Migration
  def up
    drop_table :nivel_material
    
    create_table :nivel_material do |d|
      d.string "nivel"
      d.string "material_codigo"
    end
    
  end

  def down
  end
end
