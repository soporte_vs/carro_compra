class AddTipoToPeriodo < ActiveRecord::Migration
  def self.up
    add_column :periodos, :tipo, :integer
    Periodo.connection.execute('UPDATE periodos SET tipo = 1')
    change_column :periodos, :tipo, :integer, :null => false
  end

  def self.down
    remove_column :periodos, :tipo
  end
end
