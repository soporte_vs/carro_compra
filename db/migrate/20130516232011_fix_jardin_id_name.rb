class FixJardinIdName < ActiveRecord::Migration
  def up
    rename_column :usuarios, :jardin_id, :jardin_codigo
    rename_column :solicitudes, :jardin_id, :jardin_codigo

  end

  def down
  end
end
