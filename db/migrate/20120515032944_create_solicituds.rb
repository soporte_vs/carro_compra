class CreateSolicituds < ActiveRecord::Migration
  def change
    create_table :solicitudes do |t|
      t.integer :jardin_id
      t.integer :periodo_id
      t.integer :estado
      t.timestamps
    end
  end
end
