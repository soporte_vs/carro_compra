class AddObservacionesOnSolicitudes < ActiveRecord::Migration
  def up
    add_column 'solicitudes','observacion_solicitante', :string
    add_column 'solicitudes','observacion_administrador', :string
  end

  def down
  end
end
