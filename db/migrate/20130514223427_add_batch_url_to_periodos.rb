class AddBatchUrlToPeriodos < ActiveRecord::Migration
  def change
    add_column :periodos, :batch_url, :string
  end
end
