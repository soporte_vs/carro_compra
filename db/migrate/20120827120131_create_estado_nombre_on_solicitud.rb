class CreateEstadoNombreOnSolicitud < ActiveRecord::Migration
  def up
    add_column "solicitudes", "estado_nombre", :string, :default => "CREADA", :null => false 
    
    for s in Solicitud.find(:all)
      puts "#{s.periodo_id} -> #{s.jardin_id}"
      s.save!
    end
    
  end

  def down
  end
end
