class AddDefaultValueToEstadoAttributeOnSolicitud < ActiveRecord::Migration
  def up
    change_column :solicitudes, :estado, :integer, :default => 0, :null => false
  end
end
