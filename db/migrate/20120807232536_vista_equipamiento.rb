class VistaEquipamiento < ActiveRecord::Migration
  def up
    add_column "periodos", "modo", "integer", :default => 0
    p = Periodo.find(PERIODO)
    p.modo = 1
    p.save!
  end

  def down
  end
end
