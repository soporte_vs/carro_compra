class FixPresupuestos2015 < ActiveRecord::Migration
  def up

    jp = []
    jp << [40601, 450000]
    jp << [40701,1700000]
    jp << [40702, 800000]
    jp << [40703, 400000]
    jp << [40704, 230000]
    jp << [40706,1400000]
    jp << [40708, 200000]
    jp.each do |k|
      j = Jardin.find_by_codigo(k[0])
      if j
      	p = Presupuesto.where(:jardin_id => j.id, :periodo_id => 43).first
      	p.presupuesto = k[1]
      	p.save!
      end
    end
  end

  def down
  end
end
