# -*- coding: utf-8 -*-
class AddAprobadoToPeriodos < ActiveRecord::Migration
  def change
aprobado = <<EOF
Le informamos que su solicitud luego de ser revisada por la oficina regional, ha sido <b>APROBADA</b>,
por lo que será transmitida al nivel central para que gestione la adquisición.
Le recordamos que el equipamiento será despachado a los establecimientos entre los meses de
Abril y Mayo del 2013. Muchas gracias por hacer uso de la plataforma para gestionar su pedido
EOF

    add_column :periodos, :aprobado, :text, :null => false, :default => aprobado
  end
end
