class Presupuesto < ActiveRecord::Migration
  def up
    add_column :solicitudes, :presupuesto, :integer
    
    for s in Solicitud.find(:all)
      presupuesto = 0
      for d in s.detalles
        presupuesto = presupuesto + (d.material.precio_neto * d.cantidad_propuesta)
      end
      s.presupuesto = presupuesto
      s.save!
    end
    
  end

  def down
    remove_column :solicitudes, :presupuesto
  end
end
