class CambiosOct15 < ActiveRecord::Migration
  def up
    jp = []
    jp << [82201, 555000]
    jp << [83512, 555000]
    jp.each do |k|
      j = Jardin.find_by_codigo(k[0])
      if j
      	p = Presupuesto.where(:jardin_id => j.id, :periodo_id => 43).first
      	p.presupuesto = k[1]
      	p.save!
      	s = Solicitud.where(:periodo_id => 43, :jardin_codigo => k[0]).first
      	s.estado = Solicitud.CREADA
      	s.save!
      end
    end

  end

  def down
  end
end
