class Postmarkapp < ActiveRecord::Migration
  def up
    add_column :solicitudes, :email, :string
    add_column :solicitudes, :user_id, :integer
  end

  def down
    remove_column :solicitudes, :email
    remove_column :solicitudes, :user_id
  end
end
