class AddAutorizaToUsuarios < ActiveRecord::Migration
  def up
    add_column :usuarios, :autoriza, :boolean, :default => false, :null => false


  end

  def down
    remove_column :usuarios, :autoriza
  end
end
