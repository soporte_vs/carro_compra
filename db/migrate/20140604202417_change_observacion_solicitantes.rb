class ChangeObservacionSolicitantes < ActiveRecord::Migration
  def up
    change_column :solicitudes, :observacion_solicitante, :text
  end

  def down
    change_column :solicitudes, :observacion_solicitante, :string

  end
end
