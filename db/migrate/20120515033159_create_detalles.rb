class CreateDetalles < ActiveRecord::Migration
  def change
    create_table :detalles do |t|
      t.integer :solicitud_id
      t.integer :material_id
      t.integer :cantidad_propuesta
      t.integer :cantidad_solicitada
      t.timestamps
    end
  end
end
