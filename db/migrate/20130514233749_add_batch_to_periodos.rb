class AddBatchToPeriodos < ActiveRecord::Migration
  def change
    add_column :periodos, :batch, :string
    add_column :periodos, :batch_tmp, :string
  end
end
