class Equipamiento < ActiveRecord::Migration
  def up
     add_column :materiales, :caracteristicas, :text
     add_column :materiales, :coeficienteTecnico, :text
  end

  def down
    remove_column :materiales, :caracteristicas
    remove_column :materiales, :coeficienteTecnico
  end
end
