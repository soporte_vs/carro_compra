# -*- coding: utf-8 -*-
class AddDefaultInstruccionesToPeriodos < ActiveRecord::Migration
  def change
    rename_column :periodos, :comentarios, :instrucciones
instrucciones = <<EOF
<p>Por favor ajuste las cantidades que va a solicitar de cada producto en la
columna Cantidad solicitada, de acuerdo a su necesidad de equipamiento en
el establecimiento. Recuerde que no puede exceder con su pedido el valor
que tiene asignado como presupuesto.</p>
<p>Puede guardar los cambios en la planilla si así lo desea utilizando el
botón celeste "Guardar los cambios", que se encuentra al final de la
página , pero una vez finalizado el pedido debe presionar el botón verde "Terminar el pedido",  y luego se le solicitará su correo electrónico al cual le será enviado un comprobante, una vez validado en la Dirección
Regional. Las fotografías de los productos son sólo referenciales, basadas
en compras recientes.</p>
EOF
    for p in Periodo.all
      p.nombre = "#{p.id}" if p.nombre.nil?
      p.instrucciones = instrucciones
      p.save!
    end
    change_column :periodos, :instrucciones, :text, :null => false, :default => instrucciones

  end
end
