class AddDetailsToDetalles < ActiveRecord::Migration
  def change
	add_column :detalles, :cantidad_scme, :integer, default: 0
	add_column :detalles, :cantidad_scma, :integer, default: 0
	add_column :detalles, :cantidad_sc, :integer, default: 0
        add_column :detalles, :cantidad_mme, :integer, default: 0
        add_column :detalles, :cantidad_mma, :integer, default: 0
        add_column :detalles, :cantidad_med, :integer, default: 0
        add_column :detalles, :cantidad_tme, :integer, default: 0
        add_column :detalles, :cantidad_tma, :integer, default: 0
        add_column :detalles, :cantidad_tra, :integer, default: 0
        add_column :detalles, :cantidad_het, :integer, default: 0
  end
end
