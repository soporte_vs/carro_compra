class CreatePresupuestos < ActiveRecord::Migration
  def up
    create_table :presupuestos do |t|
      t.integer :jardin_id, :null => false
      t.integer :periodo_id, :null => false
      t.integer :presupuesto, :null => false, :default => 0

      t.timestamps
    end
  end

  def down
    drop_table :presupuestos
  end
end
