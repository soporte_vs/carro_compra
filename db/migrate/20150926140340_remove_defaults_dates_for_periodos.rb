class RemoveDefaultsDatesForPeriodos < ActiveRecord::Migration
  def up
    change_column_default(:periodos, :time_start, nil)
    change_column_default(:periodos, :time_end, nil)
  end

  def down
  end
end
