require 'action_dispatch/middleware/session/dalli_store'

Integra::Application.configure do
  config.cache_classes                     = true
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  config.i18n.fallbacks                    = true
  config.active_support.deprecation        = :notify

  config.assets.initialize_on_precompile   = false
  config.serve_static_assets               = false
  config.assets.compress                   = true
  config.assets.compile                    = false
  config.assets.digest                     = true

  #bucket = (ENV['FOG_DIRECTORY']).gsub("'", '')
  #config.action_controller.asset_host = "//s3.amazonaws.com/#{bucket}"
  #config.asset_host = "//s3.amazonaws.com/##{bucket}}"

  config.threadsafe! unless defined?($rails_rake_task) && $rails_rake_task

  config.action_mailer.asset_host = 'http://carro.integra.cl'
  config.action_mailer.default_url_options = { host: 'carro.integra.cl' }

  # Raise exceptions instead of rendering exception templates
  config.action_dispatch.show_exceptions = true
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true

  config.log_level = :info

  ActionMailer::Base.smtp_settings = {
    address:        'smtp.office365.com',
    port:           587,
    authentication: 'login',
    user_name:      'carro@fundacion.integra.cl',
    password:       'Correo.2016',
    domain:         'fundacion.integra.cl',
    enable_starttls_auto: true
  }

  ActionMailer::Base.delivery_method = :smtp
  #config.cache_store = :dalli_store, "#{(ENV['MEMCACHED_HOST']).gsub("'", '')}:#{(ENV['MEMCACHED_PORT']).gsub("'", '')}"
  config.cache_store = :dalli_store

def deliver!(mail = @mail)
 raise "no mail object available for delivery!" unless mail

 #
 # Logging happens first (or not)
 #
 unless logger.nil?
#   logger.info "--------------------------------------------------------------------------------------------------------------"
   logger.info  "Enviar email a #{Array(recipients).join(', ')}"
#   logger.info "\n#{mail.encoded}"
#   logger.info "--------------------------------------------------------------------------------------------------------------"
 end

 #
 # And then we decide if an email should really get sent
 #
 begin
   __send__("perform_delivery_#{delivery_method}", mail) 
 rescue Exception => e  # Net::SMTP errors or sendmail pipe errors
   	logger.error(e)
	raise e
 end

 return mail
end






end



