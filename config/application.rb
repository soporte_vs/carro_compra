require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  Bundler.require(*Rails.groups(assets: %w(development test)))
end

module Integra
  class Application < Rails::Application
    config.time_zone = 'Buenos Aires'

    config.i18n.default_locale = :es

    config.encoding = 'utf-8'

    config.filter_parameters += [:password]

    config.active_support.escape_html_entities_in_json = true

    config.active_record.whitelist_attributes = true

    config.assets.enabled = true

    config.assets.version = '1.0'
  end
end

# TODO Fix this hardcoded variables >>
ASSET_HOST = 'http://cdn-integra.b9.cl'.freeze

NIVELES = ['SCME', 'SCMA', 'SC', 'MME', 'MMA', 'MED', 'TME', 'TMA', 'TRA', 'HET', 'E.H.SC', 'E.H.PARV', 'JSR', 'HOS', 'CA-HOG', 'JI'].freeze

PERIODO = 1.freeze
