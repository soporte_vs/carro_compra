# Be sure to restart your server when you modify this file.

#Integra::Application.config.session_store :cookie_store, :key => '_carro_integra_s', :expire_after => 24.hours

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
#Integra::Application.config.session_store :active_record_store

# require 'action_dispatch/middleware/session/dalli_store'
#
# path = File.expand_path("../../memcached.yml", __FILE__)
# puts "session_store #{path}"
# memcached = YAML.load(File.read(path))
# memcached.merge! memcached.fetch(Rails.env, {})
# memcached.symbolize_keys!
#
# if Rails.env.production?
#   Integra::Application.config.session_store :dalli_store, :memcache_server => "ip-10-181-173-32.ec2.internal:11211", :namespace => "carrointegra", :expire_after => 1.days
# else
#   Integra::Application.config.session_store :cookie_store, :key => '_carro_integra_s', :expire_after => 24.hours
# end
