Dir.mkdir("#{Rails.root}/tmp") unless File.directory?("#{Rails.root}/tmp")
Dir.mkdir("#{Rails.root}/tmp/uploads") unless File.directory?("#{Rails.root}/tmp/uploads")

if Rails.env.staging? || Rails.env.production? || Rails.env.development?
  CarrierWave.configure do |config|
    config.fog_credentials = {
      provider: 'AWS', 
      aws_access_key_id: (ENV['AWS_ACCESS_KEY_ID']).gsub("'", ''), 
      aws_secret_access_key: (ENV['AWS_SECRET_ACCESS_KEY']).gsub("'", '') 
    }
    config.fog_directory = (ENV['FOG_DIRECTORY']).gsub("'", '')
    config.fog_public = true
    config.fog_attributes = { 'Cache-Control' => 'max-age=315576000' } 
    config.cache_dir = "#{Rails.root}/tmp/uploads"
    config.store_dir = "#{Rails.root}/tmp/store/"
    config.max_file_size = 10.megabytes
  end
end

#if Rails.env.development?
#  CarrierWave.configure do |config|
#    config.storage = :file
#  end
#end
