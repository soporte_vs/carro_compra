Integra::Application.routes.draw do


  mount RedactorRails::Engine => '/redactor_rails'

  resources :jardines

  match 'materiales/modal/:id' => 'materiales#modal'
  match 'solicitudes/pendientes' => 'solicitudes#pendientes'
  resources :solicitudes, :only => :show

  match '/periodos/:periodo_id/materiales/upload' => 'materiales#upload'

  resources :periodos do
    resources :materiales
    resources :solicitudes, :except => [:show, :new]
  end

  resources :categorias
  match 'solicitudes/xls' => "solicitudes#xls"
  match 'solicitudes/:solicitud_id/aprobar' => 'solicitudes#aprobar'
  match 'solicitudes/:solicitud_id/rechazar' => 'solicitudes#rechazar'
  match 'solicitudes/new/:jardin_id' => "solicitudes#new"
  get 'jardines/:jardin/usuarios' => "usuarios#index"
  delete 'jardines/:jardin/usuarios' => "usuarios#destroy"
  match 'usuarios/upload' => 'usuarios#upload'
  get '/usuarios/:id/edit' => 'usuarios#edit'
  put '/usuarios/:id' => 'usuarios#update'
  get '/usuarios/:jardin_codigo/new' => 'usuarios#new'
  match '/usuarios/:jardin_codigo/edit' => 'usuarios#edit'
  post '/usuarios' => 'usuarios#create'
  match '/login' => 'sessions#new', :as => :login
  match '/logout' => 'sessions#destroy', :as => :logout

  resources :sessions

  resources :usuarios


  root :to => 'periodos#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
