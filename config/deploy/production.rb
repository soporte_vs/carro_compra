server '54.233.170.247', :app, :db, primary: true
#server 'ec2-18-228-43-114.sa-east-1.compute.amazonaws.com', user: 'ubuntu', roles: %w{web app}, primary: true

#Application settings
set :rails_env, "production"
set :branch, "master"
set :deploy_to, "/home/deploy/#{rails_env}.#{app_stage}"

#Database settings
set :database_adapter, "postgresql"
set :database_pool, "25"
