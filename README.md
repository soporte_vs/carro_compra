## Integra solicitudes

### Instalación

Dependencias

 * Ruby 2.1.7
 * memcached 1.4
 * Postgresql 9.4


#### Cargando base de datos de produccion en development environment

Ejemplo
```
psql -h localhost -d integra_development -U integra_user -f database.sql
```

### Iniciar servidor en modo de desarrollo
```
memcached -vvvv
rails s


### COMANDOS LEVANTAR DOCKER

sudo docker-compose up

## para hacer un build de la imagen

sudo docker-compose up --build

## para ingresar a la imagen

sudo docker exec -it CONTAINER_ID bash

## como saber el container id

sudo docker container ls

## para ver los procesos corriendo

sudo docker ps

## ya dentro de la imagen, generar la ssh de nuevo

ssh-keygen (enter a todo)

## posterior a la ssh-keygen

eval "$(ssh-agent -s)"

ssh-add ~/.ssh/id_rsa

## despues de todo, deployar el cambio en prod

cap production deploy
