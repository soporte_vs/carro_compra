# -*- coding: utf-8 -*-
require 'spec_helper'

describe 'Procesar' do
  fixtures :categorias

  it 'carga materiales fungibles' do
    periodo = create(:periodo, :tipo => 1)
    ProcesarMateriales.new(periodo.id).procesar("#{Rails.root}/db/2013/MF2013.xlsx")
  end

  it 'carga materiales didacticos' do
    periodo = create(:periodo, :tipo => 2)
    ProcesarMateriales.new(periodo.id).procesar("#{Rails.root}/db/2013/MD2014.xlsx")
  end

  it 'carga materiales fungibles' do
    periodo = create(:periodo, :tipo => 2)
    ProcesarMateriales.new(periodo.id).procesar("#{Rails.root}/db/2013/MF2014.xlsx")
  end

  it 'procesa lista de usuarios', :focus => true do
    file = "#{Rails.root}/db/2013/usuarios.xlsx"
    Usuario.actualizar_usuarios(file)

    user = Usuario.where(:username => '12458296').first
    user.region.should eql(1)
    user.autoriza.should be_false
    user.valido.should be_true
    user.jardin_codigo.should eql(10503)


    user = Usuario.where(:username => '12212642').first
    user.autoriza.should be_true
    user.jardin_codigo.should eql(0)
    user.region.should eql(1)
    user.valido.should be_true

    user = Usuario.where(:username => '14425213').first
    user.autoriza.should be_false
    user.jardin_codigo.should eql(0)
    user.region.should eql(1)
    user.valido.should be_true

  end

  it 'carga materiales equipamiento ' do
    @periodo = create(:periodo, :tipo => 3)
    ProcesarMateriales.new(@periodo.id).procesar("#{Rails.root}/db/2013/Continuidad2014.xlsx")

    Jardin.where(:codigo => '10501').first.presupuesto(@periodo.id).should eql(879000)
    Jardin.where(:codigo => '161203').first.presupuesto(@periodo.id).should eql(154000)

    Jardin.where(:codigo => '10501').first.niveles.should eql(["SCME","SCMA","MME","MMA"])

    @periodo.materiales.count.should eql(46)

    @niveles = JardinNivel.buscar_niveles(10501)
    @niveles.join(',').should eql("'SCME','SCMA','MME','MMA'")

    Material.where(:periodo_id => @periodo.id, :codigo => '03-MSC-04-0130').first.niveles.should eql(['SCMA','SC'])
    Material.where(:periodo_id => @periodo.id, :codigo => '04-EPS-04-2312').first.niveles.should eql(['MME','MED','HET'])


    @materiales = Material.where(["periodo_id = ? and ((codigo IN (select material_codigo from nivel_material where nivel IN (#{@niveles.join(',')}))))", @periodo.id]).order("categoria_id ASC, producto ASC")

    (@materiales.map {|m| m.id}).should include(Material.where(:periodo_id => @periodo.id, :codigo => '03-MSC-04-0130').first.id)



    @niveles = JardinNivel.buscar_niveles(10506)
    @niveles.join(',').should eql("'SC','HET'")


    @materiales = Material.where(["periodo_id = ? and ((codigo IN (select material_codigo from nivel_material where nivel IN (#{@niveles.join(',')}))))", @periodo.id]).order("categoria_id ASC, producto ASC")

    (@materiales.map {|m| m.id}).should include(Material.where(:periodo_id => @periodo.id, :codigo => '03-MSC-04-0130').first.id)
    (@materiales.map {|m| m.id}).should_not include(Material.where(:periodo_id => @periodo.id, :codigo => '02-JUG-01-0125').first.id)


    puts "procesando imágenes"
    Dir.mkdir("#{Rails.root}/tmp") unless File.directory?("#{Rails.root}/tmp")
    Dir.mkdir("#{Rails.root}/tmp/codigo") unless File.directory?("#{Rails.root}/tmp/codigo")

    Zip::ZipFile.open("#{Rails.root}/spec/fixtures/fotosContinuidad2014.zip") do |zipfile|
      zipfile.each do |file|
        if file.file?
          codigo = File.basename(file.to_s,File.extname(file.to_s))
          puts codigo
          materiales = Material.where(:codigo => codigo)
          first = true
          for m in materiales
            puts "procesando #{m.id} -> #{m.codigo}"
            if first
              zipfile.extract(file, "#{Rails.root}/tmp/codigo/#{File.basename(file.to_s)}"){ true }
              f = File.open("#{Rails.root}/tmp/codigo/#{File.basename(file.to_s)}")
              m.image.store!(f)
              first = false
            end
            m.has_picture = true
            m.save!
            puts "m.has_picture: #{m.has_picture}"
          end
        end
      end
    end

    Material.where(:codigo => '04-EPS-06-2551', :periodo_id => @periodo.id).first.has_picture?.should be_true



  end






end