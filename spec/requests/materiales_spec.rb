# -*- coding: utf-8 -*-
require 'spec_helper'

describe 'Materiales' do
  it 'sube materiales fungibles', :js => true do
    admin = Usuario.find_by_username('7205989')
    if admin.nil?
      admin = create(:usuario, :username => '7205989', :password => '7205', :password_confirmation => '7205')
      admin.admin = 1
      admin.save!
    end
    puts "admin id es #{admin.id}"
    admin.id.should > 0
    admin.should be_valid
    admin.isAdmin?.should eql(true)
    user = Usuario.find_by_username('7205989')
    user.valido?.should eql(true)
    user.authenticate('7205').should_not eql(false)
    Usuario.all.size.should > 0

    puts "AUsuarios size: #{Usuario.count}"



    visit '/'
    fill_in 'username', with: '7205989'
    fill_in 'password', with: '7205'
    click_button 'Ingresar'
    click_link 'Nuevo proceso de carro de compra'
    periodo = "test#{rand(999999)}"
    fill_in 'periodo_nombre', with: periodo

    choose 'Sin autorización regional, jardín posee un propuesta y puede elegir cualquier material.'

    fill_in 'periodo_time_start', with: 2.days.ago.strftime('%d-%m-%Y')
    fill_in 'periodo_time_end', with: 5.days.from_now.strftime('%d-%m-%Y')

    click_button 'Crear Periodo'

    attach_file('file',"#{Rails.root}/db/2013/MF2013.xlsx")

    click_button 'Subir listado de materiales'

    p = Periodo.where(:nombre => periodo).first
    while(p.reload.status == 0)
      puts "status 0"
      sleep 10
    end
    #
  end
end