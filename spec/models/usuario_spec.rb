# -*- coding: utf-8 -*-
require 'spec_helper'


describe "Usuario" do
  it "no puede llamarse cero", :focus => true do
    p = build(:usuario, :username => '0')

    expect {p.save!}.to raise_error(ActiveRecord::RecordInvalid)

  end
end
