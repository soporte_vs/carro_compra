# -*- coding: utf-8 -*-
require 'spec_helper'

describe Categoria do
  fixtures :categorias

  it 'can deduce the right categoria' do
    Categoria.buscar('MATERIAL DIDÁCTICO').id.should eql(5)
    Categoria.buscar('MATERIAL FUNGIBLE').id.should eql(7)
    Categoria.buscar('DIDÁCTICOS').id.should eql(5)
    input = Roo::Excelx.new("#{Rails.root}/db/2013/MD2014.xlsx")
    didactico = input.cell(2,'A')
    Categoria.buscar(didactico).id.should eql(5)
  end
end
