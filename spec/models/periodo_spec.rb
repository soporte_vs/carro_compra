# -*- coding: utf-8 -*-
require 'spec_helper'


describe "Periodo" do
  it "no debe ser activado a menos que posea un material" do
    p = create(:periodo)
    p.activo = 1
    p.activo.should eql(1)
    p.materiales.size().should eql(0)

    expect {p.save!}.to raise_error(ActiveRecord::RecordInvalid)

  end
end
