FactoryGirl.define do

  factory :periodo do |p|
    p.nombre  "Periodo"
    p.time_start 2.days.ago.strftime('%d-%m-%Y')
    p.time_end 5.days.from_now.strftime('%d-%m-%Y')
  end

  factory :usuario do |p|
    p.username "username"
    p.password "pass"
    p.nombre "nombre"
  end

end